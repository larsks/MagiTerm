#ifndef __DIALDIRECTORY_H_
#define __DIALDIRECTORY_H_

#include "Terminal.h"

struct dialinfo {
    char *name;
    char *address;
    int port;
    char *username;
	int type;
};

class dialdir {
    public:
        dialdir(Terminal *term);
        ~dialdir();
        void display();
        void process(char *data);
        void up();
        void pgup();
        void pgdown();
        void down();
        void insert();
        void backspace();
        int enter();
        void edit();
        void tab();
        char *gethost();
        int getport();
        char *getname();
        char *getuser();
        char *getpass(int item);
		int gettype();
        void savepass(int entry, char *username, char *password);
        void escape();
        void save();
        void load();
        void remove();
        void sort();
        void doexport(char *exportpath);
        void doimport(char *importpath);

    private:
        Terminal *t;
        int cur_item;
        int state;
        char tmpbbs[200];
        char tmphost[200];
		int tmptype;
        int tmpport;
		char tmpuser[200];
		char tmppass[200];
        int top;
        int editing;
        void updateCursor();
        void storepassword(int entry, char *username, char *password);
        int retrievepassword(int entry, char *username, char **password);
        void wipepassword(int entry, char *username);
        char searchdata[256];
};

#endif
