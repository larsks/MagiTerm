#!/usr/bin/env python3
print("static const unsigned char HELP_IMAGE_DATA[] = {{{}}};".format(
        ",".join(str(b) for b in open("help.bmp", "rb").read())))
