#ifdef __cplusplus
    #include <cstdlib>
#else
    #include <stdlib.h>
#endif
#include <string>
#ifdef _MSC_VER
#include <SDL.h>
#define WIN32_LEAN_AND_MEAN 1
#include <Shlobj.h>
#include <winsock2.h>
#include <ws2ipdef.h>
#include <ws2tcpip.h>
#else
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <pwd.h>
#include <SDL2/SDL.h>
#endif
#include <libssh/libssh.h>
#ifdef __linux__
#include <wait.h>
#else
#ifndef _MSC_VER
#include <sys/wait.h>
#endif
#endif
#include <sys/stat.h>
#include <limits.h>
#ifdef __sun
#include <fcntl.h>
#endif
extern "C" {
    #include "url-parser-c/url_parser.h"
}
#include "Terminal.h"
#include "filechooser.h"
#include "magiterm.h"
#include "led.h"
#include "icon.h"
#include "transfering.h"
#include "dialdirectory.h"
#include "help.h"
#include "ice.h"
#include "inih/ini.h"

#define VERSION_MAJOR 0
#define VERSION_MINOR 9
#define VERSION_PATCH 98

extern "C" {
    int zm_update = 0;
    int zm_type;
    int zm_value;
    char *zm_status = NULL;
}

struct con_info_t {
    ssh_session sess;
    char* host;
    int port;
    int* sock;
    int connected;
};

extern int send_telnet(int sock, const char* buff, int len, int flags);

#ifdef _MSC_VER
#define PATH_MAX 4096
extern std::string utf16ToUTF8(const std::wstring &s);
#define strcasecmp stricmp
int inet_pton_d(int af, const char* src, void* dst)
{
	struct sockaddr_storage ss;
	int size = sizeof(ss);
	char src_copy[INET6_ADDRSTRLEN + 1];

	ZeroMemory(&ss, sizeof(ss));
	/* stupid non-const API */
	strncpy(src_copy, src, INET6_ADDRSTRLEN + 1);
	src_copy[INET6_ADDRSTRLEN] = 0;

	if (WSAStringToAddress(src_copy, af, NULL, (struct sockaddr*) & ss, &size) == 0) {
		switch (af) {
		case AF_INET:
			*(struct in_addr*)dst = ((struct sockaddr_in*) & ss)->sin_addr;
			return 1;
		case AF_INET6:
			*(struct in6_addr*)dst = ((struct sockaddr_in6*) & ss)->sin6_addr;
			return 1;
		}
	}
	return 0;
}

const char* inet_ntop_d(int af, const void* src, char* dst, socklen_t size)
{
	struct sockaddr_storage ss;
	unsigned long s = size;

	ZeroMemory(&ss, sizeof(ss));
	ss.ss_family = af;

	switch (af) {
	case AF_INET:
		((struct sockaddr_in*) & ss)->sin_addr = *(struct in_addr*)src;
		break;
	case AF_INET6:
		((struct sockaddr_in6*) & ss)->sin6_addr = *(struct in6_addr*)src;
		break;
	default:
		return NULL;
	}
	/* cannot direclty use &size because of strict aliasing rules */
	return (WSAAddressToString((struct sockaddr*) & ss, sizeof(ss), NULL, dst, &s) == 0) ?
		dst : NULL;
}

#endif

ssh_channel chan = NULL;
int type = 0;
int tsock = 0;
SDL_Thread *dlthread;

int rz = 0;
int sz = 0;
int ft = 0;
int doublesize = 0;
int toggleToolbox = 0;
int fullscreen = 0;

extern void zmodem_upload(char *filename);

extern int recv_telnet(int sock, char* buf, int len, int flags);

SDL_Texture *loadImage(SDL_Renderer *renderer, unsigned char *iaddr, int ilen) {
    SDL_RWops *src = NULL;
    SDL_Surface *s0;
    SDL_Texture *t0;
    src = SDL_RWFromMem(iaddr, ilen);
    s0 = SDL_LoadBMP_RW(src, 1);
    t0 = SDL_CreateTextureFromSurface(renderer, s0);
    SDL_FreeSurface(s0);
    return t0;
}

SDL_Surface *loadImageSurf(unsigned char *iaddr, int ilen) {
    SDL_RWops *src = NULL;
    SDL_Surface *s0;
    src = SDL_RWFromMem(iaddr, ilen);
    s0 = SDL_LoadBMP_RW(src, 1);
    return s0;
}

int hostname_to_ip(char* hostname, char* ip) {
	struct addrinfo hints, * res, * p;
	int status;
	struct sockaddr_in* ipv4;

	memset(&hints, 0, sizeof(hints));

	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	if ((status = getaddrinfo(hostname, NULL, &hints, &res)) != 0) {
		return 1;
	}

	for (p = res; p != NULL; p = p->ai_next) {
		if (p->ai_family == AF_INET) {
			ipv4 = (struct sockaddr_in*)p->ai_addr;
#ifdef _MSC_VER
			inet_ntop_d(p->ai_family, &(ipv4->sin_addr), ip, INET_ADDRSTRLEN);
#else            
			inet_ntop(p->ai_family, &(ipv4->sin_addr), ip, INET_ADDRSTRLEN);
#endif
			freeaddrinfo(res);
			return 0;
		}
	}
	freeaddrinfo(res);
	return 1;
}


int connect_ipv4(char *server, uint16_t port, int* socketp) {
	struct sockaddr_in servaddr;
	int sock;
	char buffer[513];
	u_long iMode = 1;

#ifdef _MSC_VER
	WSADATA wsaData;
	WORD wVersionRequested;
	wVersionRequested = MAKEWORD(2, 2);

	WSAStartup(wVersionRequested, &wsaData);
#endif

	memset(&servaddr, 0, sizeof(struct sockaddr_in));
#ifdef _MSC_VER
	if (inet_pton_d(AF_INET, server, &servaddr.sin_addr) != 1) {
#else
	if (inet_pton(AF_INET, server, &servaddr.sin_addr) != 1) {
#endif
		if (hostname_to_ip(server, buffer)) {
			return -1;
		}
#ifdef _MSC_VER
		if (!inet_pton_d(AF_INET, buffer, &servaddr.sin_addr)) {
#else
		if (!inet_pton(AF_INET, buffer, &servaddr.sin_addr)) {
#endif
			return -1;
		}
	}
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(port);
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		return -1;
	}


	if (connect(sock, (struct sockaddr*) & servaddr, sizeof(servaddr)) < 0) {
		return 0;
	}
#if defined(_MSC_VER) || defined(WIN32)
	ioctlsocket(sock, FIONBIO, &iMode);
#else 
#if defined(__sun)
	int flags = fcntl(sock, F_GETFL, 0);
	fcntl(sock, F_SETFL, flags | O_NONBLOCK);
#else
	ioctl(sock, FIONBIO, &iMode);
#endif
#endif
	* socketp = sock;
	return 1;
}


static int settings_handler(void* user, const char* section, const char* name,
                   const char* value)
{
    if (strcasecmp(section, "Main") == 0) {
        if (strcasecmp(name, "fullscreen") == 0) {
            if (strcasecmp(value, "true") == 0) {
                fullscreen = 1;
            } else {
                fullscreen = 0;
            }
        }
        if (strcasecmp(name, "doublesize") == 0) {
            if (strcasecmp(value, "true") == 0) {
                doublesize = 1;
            } else {
                doublesize = 0;
            }
        }
        if (strcasecmp(name, "toolbox visible") == 0) {
            if (strcasecmp(value, "true") == 0) {
                toggleToolbox = 0;
            } else {
                toggleToolbox = 1;
            }
        }                      
    }
    return 1;
}

void save_settings() {
    char buffer[PATH_MAX];
    char *prefpath = SDL_GetPrefPath("Magicka", "MagiTerm");
    FILE *fptr;

    snprintf(buffer, PATH_MAX, "%sprefs.ini", prefpath);

    fptr = fopen(buffer, "w");

    if (fptr) {
        fprintf(fptr, "[Main]\n");
        if (fullscreen) {
            fprintf(fptr, "FullScreen = true\n");
        } else {
            fprintf(fptr, "FullScreen = false\n");
        }
        if (doublesize) {
            fprintf(fptr, "DoubleSize = true\n");
        } else {
            fprintf(fptr, "DoubleSize = false\n");
        }

        if (toggleToolbox) {
            fprintf(fptr, "Toolbox Visible = false\n");
        } else {
            fprintf(fptr, "Toolbox Visible = true\n");
        }

        fclose(fptr);
    }
    SDL_free(prefpath);
}

extern "C" FILE * zmodemlogfile;

int do_telnet_connect(void* ptr) {
    struct con_info_t* con_inf = (struct con_info_t*)ptr;
    int ret = connect_ipv4(con_inf->host, con_inf->port, con_inf->sock);
    if (ret == 1 && con_inf->connected != 0) {
#ifdef _MSC_VER
        closesocket(tsock);
        WSACleanup();
#else
        close(tsock);
#endif
        
        free(con_inf);
        return 0;
    }
    else {
        con_inf->connected = 1;
    }
    return ret;
}

int do_ssh_connect(void* ptr) {
    struct con_info_t* con_inf = (struct con_info_t*)ptr;
    int ret = ssh_connect(con_inf->sess);
    if (ret == SSH_OK && con_inf->connected != 0) {
        ssh_disconnect(con_inf->sess);
        ssh_free(con_inf->sess);
        free(con_inf);
        return 0;
    }
    con_inf->connected = 1;
    return ret;
}

int main ( int argc, char** argv )
{
	int err;
    int state = 0;
    char *host;
    char username[256];
    char *user;
    char *pass;
    char password[256];
    char buffer[4096];
    char *uploadfile;
    char knownhosts[PATH_MAX];
    char exportpath[PATH_MAX];
    int i = 0;
    int port = 0;
    int pos = 0;
	url_parser_url_t *parsed_url;
    SDL_Window *window;
    SDL_Texture *toolbox;
    SDL_Texture *help;
    SDL_Texture *led;
    SDL_Surface *icon;
    SDL_Texture *transfering;
    SDL_Renderer *renderer;
    SDL_Texture *screen;
    SDL_Texture *ice;  
    SDL_Thread* conn_thread;
    int sshstate;
    size_t hlen;
    unsigned char *hash = NULL;
    char *hexa;
    char buf[10];
    ssh_session sess;
    int first_enter = 0;
    int dragging = 0;
    int drag_start_x = 0;
    int drag_start_y = 0;
    int drag_end_x = 0;
    int drag_end_y = 0;
    int start_drag = 0;
    int endx, endy;
    int oldstate;
    int x, y;
    char *clipboard;
    Uint32 copypixel, r, b, g;
    char window_title[256];
    filechooser *fdir;
  //  int verbosity = SSH_LOG_TRACE;
    SDL_Rect dest, src;
    int zmodem = 0;
    ssh_key key;
    struct stat st;
    char *importpath;
    int toggleHelp = 0;

    int screenWidth = 640;
    int screenHeight = 480;
    SDL_DisplayMode mode;
    double scalew = 1;
    double scaleh = 1;
	unsigned char topts[3];
	char* ptr;
    struct con_info_t* con_inf;
#ifdef _MSC_VER
	WCHAR *homedir;
#else
    struct passwd *pwd;
#endif
    // initialize SDL video
    if ( SDL_Init( SDL_INIT_VIDEO | SDL_INIT_EVENTS ) < 0 )
    {
        printf( "Unable to init SDL: %s\n", SDL_GetError() );
        return 1;
    }

	memset(buf, 0, 10);
	memset(username, 0, 256);
	memset(password, 0, 256);

    // make sure SDL cleans up before exit
    atexit(SDL_Quit);

    // create a new window
    snprintf(window_title, 256, "MagiTerm v%d.%d.%d", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH);
    window = SDL_CreateWindow(window_title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, SDL_WINDOW_SHOWN);
    renderer = SDL_CreateRenderer(window, -1, 0);

    // load an image

    toolbox = loadImage(renderer, (unsigned char *)TOOLBOX_IMAGE_DATA, sizeof(TOOLBOX_IMAGE_DATA));
    led = loadImage(renderer, (unsigned char *)LED_IMAGE_DATA, sizeof(LED_IMAGE_DATA));
    help = loadImage(renderer, (unsigned char *)HELP_IMAGE_DATA, sizeof(HELP_IMAGE_DATA));
    icon = loadImageSurf((unsigned char *)ICON_IMAGE_DATA, sizeof(ICON_IMAGE_DATA));
    transfering = loadImage(renderer, (unsigned char *)TRANSFERING_IMAGE_DATA, sizeof(TRANSFERING_IMAGE_DATA));
    ice = loadImage(renderer, (unsigned char *)ICE_IMAGE_DATA, sizeof(ICE_IMAGE_DATA));
    SDL_SetWindowIcon(window, icon);

    char *prefpath = SDL_GetPrefPath("Magicka", "MagiTerm");
    snprintf(buffer, 4096, "%sprefs.ini", prefpath);
	SDL_free(prefpath);

    ini_parse(buffer, settings_handler, NULL);

    if (fullscreen) {
        SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
        SDL_GetWindowDisplayMode(window, &mode);
        screenWidth = mode.w;
        screenHeight = mode.h;
        scalew = (double)screenWidth / 640.;
        scaleh = (double)screenHeight / 480.;
    } else if (doublesize) {
        screenWidth = 1280;
        screenHeight = 960;
        scalew = 2;
        scaleh = 2;
        if (toggleToolbox) {
            SDL_SetWindowSize(window, screenWidth, 800);
        } else {
            SDL_SetWindowSize(window, screenWidth, screenHeight);
        }
    } else if (toggleToolbox) {
        SDL_SetWindowSize(window, 640, 400);
    }

    Terminal *term = new Terminal(window);

    dialdir *ddir = new dialdir(term);
    ddir->load();
    
#ifdef __APPLE__
    ddir->display();
#else
    char hostbuffer[256];
    if (argc > 1) {
        parsed_url = (url_parser_url_t *) malloc(sizeof(url_parser_url_t));
        if (!parsed_url) {
            exit(-1);
        }
        memset(parsed_url, 0, sizeof(url_parser_url_t));

		err = parse_url(argv[1], false, parsed_url);
        
        if (err != 0) {
            exit(-1);
        } else if (parsed_url->host == NULL || parsed_url->protocol == NULL) {
            free_parsed_url(parsed_url);
            ddir->display();
        } else {
            strncpy(hostbuffer, parsed_url->host, 256);
            host = hostbuffer;
            state = 2;
            
            port = parsed_url->port;
            if (strcasecmp(parsed_url->protocol, "ssh") == 0) {
                if (port == 0) {
                    port = 22;
                }

                free_parsed_url(parsed_url);

                type = 0;
                state = 2;
                term->twrite((unsigned char *)"\r\n\x1b[0m", 6);

                term->twrite((unsigned char *)"USER: ", 6);                
            } else if (strcasecmp(parsed_url->protocol, "telnet") == 0) {
                if (port == 0) {
                    port = 23;
                }
                free_parsed_url(parsed_url);
                type = 1;
                state = 90;
                term->twrite((unsigned char*)"\x1b[0m\x1b[2J\x1b[1;1H\r\nCONNECTING...\r\n", 31);
            } else {
                free_parsed_url(parsed_url);
                ddir->display();
            }
        }
    } else {
        ddir->display();
    }
#endif
    // program main loop
    bool done = false;

    SDL_StartTextInput();

    while (!done)
    {
        // message processing loop
        SDL_Event event;
             
        while (SDL_PollEvent(&event))
        {
            // check for messages
            switch (event.type)
            {
                // exit if the window is closed
            case SDL_QUIT:
                done = true;
                break;
            case SDL_MOUSEBUTTONDOWN:
                {
                    if (dragging == 0 && start_drag == 0) {
                        if (event.button.button == SDL_BUTTON_LEFT) {
                            start_drag = 1;
                            SDL_GetMouseState(&x, &y);
                            if (fullscreen) {
                                if (toggleToolbox) {
                                    drag_start_x = x / scalew;
                                    drag_start_y = y / ((screenHeight + (80 * scaleh)) / 480);
                                    drag_start_x -= drag_start_x % 8;
                                    drag_start_y -= drag_start_y % 16;
                                    drag_end_x = drag_start_x + 8;
                                    drag_end_y = drag_start_y + 16;
                                } else {
                                    if (y < 400 * scaleh) {
                                        drag_start_x = x / scalew;
                                        drag_start_y = y / scaleh;
                                        drag_start_x -= drag_start_x % 8;
                                        drag_start_y -= drag_start_y % 16;
                                        drag_end_x = drag_start_x + 8;
                                        drag_end_y = drag_start_y + 16;              
                                    }
                                }
                            } else {
                                if (doublesize && y < 800) {
                                    drag_start_x = x / scalew;
                                    drag_start_y = y / scaleh;
                                    drag_start_x -= drag_start_x % 8;
                                    drag_start_y -= drag_start_y % 16;
                                    drag_end_x = drag_start_x + 8;
                                    drag_end_y = drag_start_y + 16;
                                } else if (!doublesize && y < 400) {
                                    drag_start_x = x / scalew;
                                    drag_start_y = y / scaleh;                                
                                    drag_start_x -= drag_start_x % 8;
                                    drag_start_y -= drag_start_y % 16;
                                    drag_end_x = drag_start_x + 8;
                                    drag_end_y = drag_start_y + 16;
                                }
                            }                            
                        }
                    }

                }
                break;
            case SDL_MOUSEBUTTONUP:
                {
                    start_drag = 0;
                    if (event.button.button == SDL_BUTTON_RIGHT) {
                        if (SDL_HasClipboardText()) {
                            clipboard = SDL_GetClipboardText();
                            if (clipboard != NULL) {
                                if (state == 9) {
									if (type == 0) {
										if (!ssh_channel_is_eof(chan) && ssh_channel_is_open(chan)) {
											sz = 1;
											i = ssh_channel_write(chan, clipboard, strlen(clipboard));
											if (i != strlen(clipboard)) {
												state = 10;
											}
										}
									}
									else if (type == 1) {
										sz = 1;
										i = send(tsock, clipboard, strlen(clipboard), 0);
										if (i != strlen(clipboard)) {
											state = 10;
										}
									}
                                }             
                                SDL_free(clipboard);
                            }
                        }
                    }
                    if (dragging == 1) {
                        if (event.button.button == SDL_BUTTON_LEFT) {
                            if (drag_start_x < drag_end_x) {
                                x = drag_start_x;
                                endx = drag_end_x;
                            } else {
                                x = drag_end_x;
                                endx = drag_start_x;
                            }
                            if (drag_start_y < drag_end_y) {
                                y = drag_start_y;
                                endy = drag_end_y;
                            } else {
                                y = drag_end_y;
                                endy = drag_start_y;
                            }
                            clipboard = term->gettext(x / 8, y / 16, endx / 8, endy / 16);
                            if (clipboard != NULL) {
                                SDL_SetClipboardText(clipboard);
                                free(clipboard);
                            }
                            dragging = 0;
                        }
                    } else {
                        if (event.button.button == SDL_BUTTON_LEFT) {
                            term->clickLink(drag_start_x / 8, drag_start_y / 16);
                        }
                    }
                }
                break;
            case SDL_MOUSEMOTION:
                {
                    if (start_drag == 1 || dragging == 1) {
                        dragging = 1;
                        start_drag = 0;
                        SDL_GetMouseState(&x, &y);
                        if (fullscreen) {
                            if (toggleToolbox) {
                                if (y < 480 * scaleh) {                        
                                    drag_end_x = x / scalew;
                                    drag_end_y = y / ((screenHeight + (80 * scaleh)) / 480);
                                    drag_end_x -= drag_end_x % 8;
                                    drag_end_y -= drag_end_y % 16;
                                    drag_end_x += 8;
                                    drag_end_y += 16;                                    
                                }
                            } else {
                                if (y < 400 * scaleh) { 
                                    drag_end_x = x / scalew;
                                    drag_end_y = y / scaleh;
                                    drag_end_x -= drag_end_x % 8;
                                    drag_end_y -= drag_end_y % 16;
                                    drag_end_x += 8;
                                    drag_end_y += 16;                                                                    
                                }
                            }
                        } else {
                            drag_end_x = x / scalew;
                            drag_end_y = y / scaleh;
                            drag_end_x -= drag_end_x % 8;
                            drag_end_y -= drag_end_y % 16;
                            drag_end_x += 8;
                            drag_end_y += 16;
                        }
                    }
                }
                break;
            case SDL_KEYUP:
                {
                    if (first_enter == 0 && event.key.keysym.sym == SDLK_RETURN) {
                        first_enter = 1;
                    }
                    if (state == 0 && event.key.keysym.sym == SDLK_e && SDL_GetModState() & KMOD_CTRL) {
                        ddir->edit();
                    }      
					if (state == 0 && event.key.keysym.sym == SDLK_i && SDL_GetModState() & KMOD_CTRL) {
						ddir->insert();
					}
               
                }
                break;
            case SDL_KEYDOWN:
                {
                    if (first_enter == 0 && event.key.keysym.sym != SDLK_RETURN) {
                        first_enter = 1;
                    }

                    if (event.key.keysym.sym == SDLK_s && SDL_GetModState() & KMOD_ALT) {
                        term->screenshot();
                    }

                    if (event.key.keysym.sym == SDLK_z && SDL_GetModState() & KMOD_ALT) {
                        toggleHelp = !toggleHelp;
                    }

                    if (event.key.keysym.sym == SDLK_c && SDL_GetModState() & KMOD_ALT) {
                        term->toggleIceColours();
                    }
                    
                    if (event.key.keysym.sym == SDLK_h && SDL_GetModState() & KMOD_ALT) {
						// disconnect
						if (state == 9 || state == 11 || state == 12) {
							if (type == 1) {
#ifdef _MSC_VER
								closesocket(tsock);
#else
								close(tsock);
#endif
							}
							state = 10;
                        }
                        else if (state == 41) {
                            // ssh in process of connecting
                            con_inf->connected = 2;
                            SDL_DetachThread(conn_thread);
                            state = 0;
                            ddir->display();
                        }
                        else if (state == 91) {
                            // telnet in process of connecting
                            con_inf->connected = 2;
                            SDL_DetachThread(conn_thread);
                            state = 0;
                            ddir->display();
                        }
					}

                    if (state == 0 && event.key.keysym.sym == SDLK_INSERT) {
                        ddir->insert();
                    }
                    
                    if (state == 0 && event.key.keysym.sym == SDLK_UP) {
                        ddir->up();
                    }
                    if (state == 0 && event.key.keysym.sym == SDLK_DOWN) {
                        ddir->down();
                    }
                    if (state == 0 && event.key.keysym.sym == SDLK_PAGEUP) {
                        ddir->pgup();
                    }
                    if (state == 0 && event.key.keysym.sym == SDLK_PAGEDOWN) {
                        ddir->pgdown();
                    }
                                        
                    if (state == 0 && event.key.keysym.sym == SDLK_TAB) {
                        ddir->tab();
                    }

                    if (state == 0 && event.key.keysym.sym == SDLK_DELETE) {
                        ddir->remove();
                    }

                    if (state == 9 && event.key.keysym.sym == SDLK_DELETE) {
						if (type == 0) {
							ssh_channel_write(chan, "\x1b[3~", 4);
						}
						else if (type == 1) {
							send(tsock, "\x1b[3~", 4, 0);
						}
                    }
                    if (state == 9 && event.key.keysym.sym == SDLK_INSERT) {
						if (type == 0) {
							ssh_channel_write(chan, "\x1b[2~", 4);
						}
						else if (type == 1) {
							send(tsock, "\x1b[2~", 4, 0);
						}
                    }
                    if ((state == 9 || state == -1) && event.key.keysym.sym == SDLK_b && SDL_GetModState() & KMOD_ALT) {
                        term->scrollback();
                        oldstate = state;
                        state = 17;
                    } else if (state == 17 && event.key.keysym.sym == SDLK_b && SDL_GetModState() & KMOD_ALT) {
                        term->scrollback();
                        state = oldstate;
                    }     

                    if (state == 17 && event.key.keysym.sym == SDLK_UP) {
                        term->sb_up();
                    }
                    if (state == 17 && event.key.keysym.sym == SDLK_DOWN) {
                        term->sb_down();
                    }

                    if ((state == 14 || state == 16) && event.key.keysym.sym == SDLK_UP) {
                        fdir->up();
                        fdir->draw();
                    }
                    if ((state == 14 || state == 16) && event.key.keysym.sym == SDLK_DOWN) {
                        fdir->down();
                        fdir->draw();
                    }

                    if ((state == 14 || state == 16) && event.key.keysym.sym == SDLK_PAGEUP) {
                        fdir->pgup();
                        fdir->draw();
                    }
                    if ((state == 14 || state == 16) && event.key.keysym.sym == SDLK_PAGEDOWN) {
                        fdir->pgdown();
                        fdir->draw();
                    }

                    if ((state == 14 || state == 16) && event.key.keysym.sym == SDLK_TAB) {
                        fdir->tab();
                        fdir->draw();
                    }
                    
                    if (state == 9 && event.key.keysym.sym == SDLK_TAB) {
						if (type == 0) {
							ssh_channel_write(chan, "\t", 1);
						}
						else {
							send(tsock, "\t", 1, 0);
						}
                    }

                    if (state == 0 && event.key.keysym.sym == SDLK_e && SDL_GetModState() & KMOD_ALT) {
                        // export
#ifdef _MSC_VER
                        SHGetKnownFolderPath(FOLDERID_Documents, 0, NULL, &homedir);
                        snprintf(exportpath, PATH_MAX, "%s\\bbs_list.ini", utf16ToUTF8(homedir).c_str());
#else
                        pwd = getpwuid(getuid());
                        snprintf(exportpath, PATH_MAX, "%s/bbs_list.ini", pwd->pw_dir);
#endif
                        term->twrite((unsigned char *)"\x1b[2J\x1b[0mDefault export path is:\r\n\r\n", 35);
                        term->twrite((unsigned char *)exportpath, strlen(exportpath));
                        term->twrite((unsigned char *)"\r\n\r\nEnter a new path, or just press enter to accept the default\r\n:", 66);
                        pos = 0;
                        state = 15;
                    }

                    if (state == 0 && event.key.keysym.sym == SDLK_i && SDL_GetModState() & KMOD_ALT) {
                        // import
                        fdir = new filechooser(term);
                        fdir->load();
                        fdir->draw();
                        state = 16;
                    }

					if (state == 9 && event.key.keysym.sym == SDLK_u && SDL_GetModState() & KMOD_ALT) {
						ptr = ddir->getuser();
						if (ptr != NULL) {
							if (type == 0) {
								ssh_channel_write(chan, ptr, strlen(ptr));
								ssh_channel_write(chan, "\r", 1);
							}
							else if (type == 1) {
								send_telnet(tsock, ptr, strlen(ptr), 0);
								send_telnet(tsock, "\r", 1, 0);
							}
						}
					}

					if (state == 9 && event.key.keysym.sym == SDLK_p && SDL_GetModState() & KMOD_ALT) {
						ptr = ddir->getpass(-1);
						if (ptr != NULL) {
							if (type == 0) {
								ssh_channel_write(chan, ptr, strlen(ptr));
								ssh_channel_write(chan, "\r", 1);
							}
							else if (type == 1) {
								send_telnet(tsock, ptr, strlen(ptr), 0);
								send_telnet(tsock, "\r", 1, 0);
							}
							free(ptr);
						}
					}

                    if (event.key.keysym.sym == SDLK_x && SDL_GetModState() & KMOD_ALT) {
                        done = true;
                    }
/*
                    if (event.key.keysym.sym == SDLK_u && SDL_GetModState() & KMOD_ALT) {
                        fdir = new filechooser(term);
                        fdir->load();
                        fdir->draw();
                        state = 14;
                    }
*/

                    if (event.key.keysym.sym == SDLK_t && SDL_GetModState() & KMOD_ALT) {
                        toggleToolbox = !toggleToolbox;

                        if (!fullscreen) {
                            if (doublesize) {
                                if (!toggleToolbox) {
                                    SDL_SetWindowSize(window, screenWidth, 960);
                                } else {
                                    SDL_SetWindowSize(window, screenWidth, 800);
                                }
                            } else {
                                if (!toggleToolbox) {
                                    SDL_SetWindowSize(window, screenWidth, 480);
                                } else {
                                    SDL_SetWindowSize(window, screenWidth, 400);
                                }
                            }
                        } else {
                           // if (toggleToolbox) {
                                scaleh = (double)screenHeight / 480.;
                           // } else {
                           //     scaleh = screenHeight / 400;
                           // }
                        }
                    }

                    if (event.key.keysym.sym == SDLK_d && SDL_GetModState() & KMOD_ALT) {
                        doublesize = !doublesize;
                        if (!fullscreen) {
                            if (doublesize) {
                                screenWidth = 1280;
                                screenHeight = 960;
                                scaleh = 2;
                                scalew = 2;                                
                                if (toggleToolbox) {
                                    SDL_SetWindowSize(window, screenWidth, 800);
                                } else {
                                    SDL_SetWindowSize(window, screenWidth, screenHeight);
                                }
                            } else {
                                screenWidth = 640;
                                screenHeight = 480;
                                scaleh = 1;
                                scalew = 1;
                                if (toggleToolbox) {
                                    SDL_SetWindowSize(window, screenWidth, 400);
                                } else {
                                    SDL_SetWindowSize(window, screenWidth, screenHeight);
                                }                                
                            }
                        }
                    }

                    if (event.key.keysym.sym == SDLK_f && SDL_GetModState() & KMOD_ALT) {
                        fullscreen = !fullscreen;
                        if (!fullscreen) {
                            SDL_SetWindowFullscreen(window, 0);
                            if (doublesize) {
                                screenWidth = 1280;
                                screenHeight = 960;
                                scalew = 2;
                                scaleh = 2;
                                if (toggleToolbox) {
                                    SDL_SetWindowSize(window, screenWidth, 800);
                                } else {
                                    SDL_SetWindowSize(window, screenWidth, screenHeight);
                                }
                            } else {
                                screenWidth = 640;
                                screenHeight = 480;
                                scalew = 1;
                                scaleh = 1;
                                if (toggleToolbox) {
                                    SDL_SetWindowSize(window, screenWidth, 400);
                                } else {
                                    SDL_SetWindowSize(window, screenWidth, screenHeight);
                                }                                
                            }
                                              
                        } else {
                            SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
                            SDL_GetWindowDisplayMode(window, &mode);
                            screenWidth = mode.w;
                            screenHeight = mode.h;
                            scalew = (double)screenWidth / 640.;
                            scaleh = (double)screenHeight / 480.;
   
                        }
                    }

                    if (SDL_GetModState() & KMOD_CTRL && state == 9) {
                        if (event.key.keysym.sym >= 'a' && event.key.keysym.sym <= 'z') {
                            snprintf(buf, 10, "%c", event.key.keysym.sym  - 'a' + 1);
							if (type == 0) {
								ssh_channel_write(chan, buf, 1);
							}
							else if (type == 1) {
								send(tsock, buf, 1, 0);
							}
                            sz = 1;
                        }
                    }

                    if( event.key.keysym.sym == SDLK_BACKSPACE && state != 0 && pos != 0) {
                        //lop off character
                         if (state == 2) {
                            pos--;
                            username[pos] = '\0';
                        } else if (state == 3) {
                            pos--;
                            password[pos] = '\0';
                        } else if (state == 6) {
                            pos--;
                            buf[pos] = '\0';
                        } else if (state == 15) {
                            pos--;
                            exportpath[pos] = '\0';
                        }
                        if (state != 0) {
                            term->twrite((unsigned char *)"\x1b[D \x1b[D", 7);
                        }
                    } else if (event.key.keysym.sym == SDLK_BACKSPACE && state == 9) {
						if (type == 0) {
							ssh_channel_write(chan, "\b", 1);
						}
						else if (type == 1) {
							send(tsock, "\b", 1, 0);
						}
                        sz = 1;
                    } else if (event.key.keysym.sym == SDLK_BACKSPACE && state == 0) {
                        ddir->backspace();
                    }

                    if (event.key.keysym.sym == SDLK_RETURN || event.key.keysym.sym == SDLK_KP_ENTER) {
                        if (first_enter == 1) {
                            if (state == 0) {
                                if (ddir->enter() == 1) {
                                    host = ddir->gethost();
                                    port = ddir->getport();
                                    user = ddir->getuser();
									type = ddir->gettype();
                                    if (host != NULL) {
                                        snprintf(window_title, 256, "MagiTerm v%d.%d.%d: %s", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH, ddir->getname());
                                        SDL_SetWindowTitle(window, window_title);

										if (type == 0) {

											if (user == NULL) {
												term->twrite((unsigned char*)"\x1b[0m\x1b[2J\x1b[1;1H\r\nUSER: ", 22);
												state = 2;
											}
											else {
												pass = ddir->getpass(-1);
												if (pass == NULL) {
													strcpy(username, user);
													term->twrite((unsigned char*)"\x1b[0m\x1b[2J\x1b[1;1H\r\nPASS: ", 22);
													state = 3;
												}
												else {
													strcpy(username, user);
													strcpy(password, pass);
													free(pass);
													term->twrite((unsigned char*)"\x1b[0m\x1b[2J\x1b[1;1H\r\nCONNECTING...\r\n", 31);
													state = 4;
												}
											}
										}
										else if (type == 1) {
											// telnet connect
											term->twrite((unsigned char*)"\x1b[0m\x1b[2J\x1b[1;1H\r\nCONNECTING...\r\n", 31);
											state = 90;
										}
                                    }
                                }
                            } else if (state == 2) {
                                if (pos == 0) {
                                    state = 0;
                                    ddir->display();
                                } else {
                                    state = 3;
                                    pos = 0;
                                    term->twrite((unsigned char *)"\r\nPASS: ", 8); 
                                }
                            } else if (state == 3) {
                                if (pos == 0) {
                                    state = 0;
                                    ddir->display();
                                } else {
                                    pos = 0;
#ifndef __APPLE__                                    
                                    if (host == hostbuffer) {
                                        term->twrite((unsigned char *)"\r\nCONNECTING...\r\n", 17);
                                        state = 4;
                                    } else {
#endif
                                        term->twrite((unsigned char *)"\r\nRemember? (Yes/No): ", 22); 
                                        state = 18;
#ifndef __APPLE__                                        
                                    }
#endif
                                }
                            } else if (state == 15) {
                                pos = 0;
                                // do export
                                ddir->doexport(exportpath);
                                state = 0;
                                ddir->display();
                            } else if (state == 6 && pos != 0) {
                                state = 7;
                                pos = 0;
                            } else if (state == 9) {
								if (type == 0) {
									ssh_channel_write(chan, "\r", 1);
								}
								else if (type == 1) {
									send(tsock, "\r", 1, 0);
								}
                                sz = 1;
                            } else if (state == 14) {
                                uploadfile = fdir->enter();
                                if (uploadfile != NULL) {
                                    delete fdir;
                                    if (stat(uploadfile, &st) != 0) {
                                        term->twrite((unsigned char *)"\r\nFILE NOT FOUND...\r\n", 21);
                                        state = 9;
                                        pos = 0;
                                        free(uploadfile);
                                    } else {
                                        term->twrite((unsigned char *)"\x1b[2JUPLOADING...\r\n", 18);
                                        pos = 0;
                                        state = 11;
                                        snprintf(buffer, sizeof buffer, "\x1b[0;46;30m\x1b[8;18H+-----------------------------------------+");
                                        term->twrite((unsigned char *)buffer, strlen(buffer));
                                        snprintf(buffer, sizeof buffer, "\x1b[9;18H|                                         |");
                                        term->twrite((unsigned char *)buffer, strlen(buffer));
                                        snprintf(buffer, sizeof buffer, "\x1b[10;18H|  ZModem Status:                         |");
                                        term->twrite((unsigned char *)buffer, strlen(buffer));
                                        snprintf(buffer, sizeof buffer, "\x1b[11;18H|                                         |");
                                        term->twrite((unsigned char *)buffer, strlen(buffer));
                                        snprintf(buffer, sizeof buffer, "\x1b[12;18H|                                         |");
                                        term->twrite((unsigned char *)buffer, strlen(buffer));
                                        snprintf(buffer, sizeof buffer, "\x1b[13;18H|                                         |");
                                        term->twrite((unsigned char *)buffer, strlen(buffer));
                                        snprintf(buffer, sizeof buffer, "\x1b[14;18H|                                         |");
                                        term->twrite((unsigned char *)buffer, strlen(buffer));
                                        snprintf(buffer, sizeof buffer, "\x1b[15;18H|                                         |");
                                        term->twrite((unsigned char *)buffer, strlen(buffer));
                                        snprintf(buffer, sizeof buffer, "\x1b[16;18H|                                         |");
                                        term->twrite((unsigned char *)buffer, strlen(buffer));
                                        snprintf(buffer, sizeof buffer, "\x1b[17;18H+-----------------------------------------+\x1b[0m");
                                        term->twrite((unsigned char *)buffer, strlen(buffer));                                        
                                        zmodem_upload(uploadfile);
                                        free(uploadfile);
                                    }
                                } else {
                                    fdir->draw();
                                }
                            } else if (state == 16) {
                                importpath = fdir->enter();
                                if (importpath != NULL) {
                                    delete fdir;
                                    if (stat(importpath, &st) != 0) {
                                        state = 0;
                                        ddir->display();
                                        free(importpath);
                                    } else {
                                        state = 0;
                                        ddir->doimport(importpath);
                                        free(importpath);
                                        ddir->display();
                                    }
                                } else {
                                    fdir->draw();
                                }
                            } else if (state == 18) {
                                if (strcasecmp(buf, "yes") == 0) {
                                    ddir->savepass(-1, username, password);
                                }
                                state = 4;
                                pos = 0;
                                term->twrite((unsigned char *)"\r\nCONNECTING...\r\n", 17);
                            }
                        }
                    }

                    if (state == 0 && event.key.keysym.sym == SDLK_ESCAPE) {
                        ddir->escape();
                    }

                    if (state == 14 && event.key.keysym.sym == SDLK_ESCAPE) {
                        delete fdir;
                        term->twrite((unsigned char *)"\x1b[2JFILE UPLOAD CANCELED...\r\n", 29);
                        state = 9;
                    }

                    if (state == 16 && event.key.keysym.sym == SDLK_ESCAPE) {
                        delete fdir;
                        state = 0;
                        ddir->display();
                    }

                    if (state == -1 && event.key.keysym.sym == SDLK_ESCAPE) {
                        snprintf(window_title, 256, "MagiTerm v%d.%d.%d", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH);
                        SDL_SetWindowTitle(window, window_title);
                        ddir->display();
                        state = 0;
                    }                    

                    if (state == 9) {
                        if (event.key.keysym.sym == SDLK_ESCAPE) {
							if (type == 0) {
								ssh_channel_write(chan, "\x1b", 1);
							}
							else if (type == 1) {
								send(tsock, "\x1b", 1, 0);
							}
                            sz = 1;
                        }
                        if (event.key.keysym.sym == SDLK_UP) {
							if (type == 0) {
								ssh_channel_write(chan, "\x1b[A", 3);
							}
							else if (type == 1) {
								send(tsock, "\x1b[A", 3, 0);
							}

                            sz = 1;
                        }
                        if (event.key.keysym.sym == SDLK_DOWN) {
							if (type == 0) {
	                            ssh_channel_write(chan, "\x1b[B", 3);
							}
							else if (type == 1) {
								send(tsock, "\x1b[B", 3, 0);
							}
                            sz = 1;
                        }     
                        if (event.key.keysym.sym == SDLK_LEFT) {
							if (type == 0) {
	                            ssh_channel_write(chan, "\x1b[D", 3);
							}
							else if (type == 1) {
								send(tsock, "\x1b[D", 3, 0);
							}

                            sz = 1;
                        }
                        if (event.key.keysym.sym == SDLK_RIGHT) {
							if (type == 0) {
	                            ssh_channel_write(chan, "\x1b[C", 3);
							}
							else if (type == 1) {
								send(tsock, "\x1b[C", 3, 0);
							}

                            sz = 1;
                        }                             
                        if (event.key.keysym.sym == SDLK_END) {
							if (type == 0) {
	                            ssh_channel_write(chan, "\x1b[K", 3);
							}
							else if (type == 1) {
								send(tsock, "\x1b[K", 3, 0);
							}

                            sz = 1;
                        }
                        if (event.key.keysym.sym == SDLK_HOME) {
							if (type == 0) {
								ssh_channel_write(chan, "\x1b[H", 3);
							}
							else if (type == 1) {
								send(tsock, "\x1b[H", 3, 0);
							}

                            sz = 1;
                        }                        
                        if (event.key.keysym.sym == SDLK_PAGEUP) {
							if (type == 0) {
								ssh_channel_write(chan, "\x1b[V", 3);
							}
							else if (type == 1) {
								send(tsock, "\x1b[V", 3, 0);
							}

                            sz = 1;
                        }                        
                        if (event.key.keysym.sym == SDLK_PAGEDOWN) {
							if (type == 0) {
	                            ssh_channel_write(chan, "\x1b[U", 3);
							}
							else if (type == 1) {
								send(tsock, "\x1b[U", 3, 0);
							}


                            sz = 1;
                        }
                        if (event.key.keysym.sym == SDLK_F1) {
							if (type == 0) {
								ssh_channel_write(chan, "\x1b[OP", 4);
							}
							else if (type == 1) {
								send(tsock, "\x1b[OP", 4, 0);
							}
                            sz = 1;
                        }                    
                        if (event.key.keysym.sym == SDLK_F2) {
                            if (type == 0) {
								ssh_channel_write(chan, "\x1b[OQ", 4);
							}
							else if (type == 1) {
								send(tsock, "\x1b[OQ", 4, 0);
							}

                            sz = 1;
                        }                    
                        if (event.key.keysym.sym == SDLK_F3) {
							if (type == 0) {
	                            ssh_channel_write(chan, "\x1b[OR", 4);
							}
							else if (type == 1) {
								send(tsock, "\x1b[OR", 4, 0);
							}

                            sz = 1;
                        }                    
                        if (event.key.keysym.sym == SDLK_F4) {
							if (type == 0) {
								ssh_channel_write(chan, "\x1b[OS", 4);
							}
							else if (type == 1) {
								send(tsock, "\x1b[OS", 4, 0);
							}

                            sz = 1;
                        }                                            
                        if (event.key.keysym.sym == SDLK_F5) {
							if (type == 0) {
								ssh_channel_write(chan, "\x1b[[15~", 6);
							}
							else if (type == 1) {
								send(tsock, "\x1b[[15~", 6, 0);
							}
                            sz = 1;
                        }                    
                        if (event.key.keysym.sym == SDLK_F6) {
							if (type == 0) {
	                            ssh_channel_write(chan, "\x1b[[17~", 6);
							}
							else if (type == 1) {
								send(tsock, "\x1b[[17~", 6, 0);
							}
                            sz = 1;
                        }                    
                        if (event.key.keysym.sym == SDLK_F7) {
							if (type == 0) {
	                            ssh_channel_write(chan, "\x1b[[18~", 6);
							}
							else if (type == 1) {
								send(tsock, "\x1b[[18~", 6, 0);
							}

                            sz = 1;
                        }                    
                        if (event.key.keysym.sym == SDLK_F8) {
							if (type == 0) {
	                            ssh_channel_write(chan, "\x1b[[19~", 6);
							}
							else if (type == 1) {
								send(tsock, "\x1b[[19~", 6, 0);
							}

                            sz = 1;
                        }                    
                        if (event.key.keysym.sym == SDLK_F9) {
							if (type == 0) {
								ssh_channel_write(chan, "\x1b[[20~", 6);
							}
							else if (type == 1) {
								send(tsock, "\x1b[[20~", 6, 0);
							}

                            sz = 1;
                        }                    
                        if (event.key.keysym.sym == SDLK_F10) {
							if (type == 0) {
								ssh_channel_write(chan, "\x1b[[21~", 6);
							}
							else if (type == 1) {
								send(tsock, "\x1b[[21~", 6, 0);
							}

                            sz = 1;
                        }                    
                        if (event.key.keysym.sym == SDLK_F11) {
							if (type == 0) {
	                            ssh_channel_write(chan, "\x1b[[23~", 6);
							}
							else if (type == 1) {
								send(tsock, "\x1b[[23~", 6, 0);
							}

                            sz = 1;
                        }                    
                        if (event.key.keysym.sym == SDLK_F12) {
							if (type == 0) {
	                            ssh_channel_write(chan, "\x1b[[24~", 6);
							}
							else if (type == 1) {
								send(tsock, "\x1b[[24~", 6, 0);
							}

                            sz = 1;
                        }
                    }
                }
                break;
            case SDL_TEXTINPUT:
                {
                    if (!(SDL_GetModState() & KMOD_CTRL) && !(SDL_GetModState() & KMOD_ALT)) {
                        if (state == 0) {
                            ddir->process(event.text.text);
                        } else if (state == 2) {
                            if (strlen(username) + strlen(event.text.text) > 255) {
                                state = 3;
                                pos = 0;
                                term->twrite((unsigned char *)"\r\nPASS: ", 8);
                            } else {
                                for (i=0;i<strlen(event.text.text);i++) {
                                    username[pos++] = event.text.text[i];
                                    username[pos] = '\0';
                                    term->twrite((unsigned char *)&event.text.text[i], 1);
                                }
                                
                            }
                        } else if (state == 3) {
                            if (strlen(password) + strlen(event.text.text) > 255) {
                                state = 4;
                                pos = 0;
                                term->twrite((unsigned char *)"\r\n\r\nCONNECTING...\r\n", 19);
                            } else {
                                for (i=0;i<strlen(event.text.text);i++) {
                                    password[pos++] = event.text.text[i];
                                    password[pos] = '\0';
                                    term->twrite((unsigned char *)"*", 1);
                                }
                            }
                        } else if (state == 6) {
                            if (strlen(buf) + strlen(event.text.text) >  9) {
                                state = 7;
                                pos = 0;
                            } else {
                                for (i=0;i<strlen(event.text.text);i++) {
                                    buf[pos++] = event.text.text[i];
                                    buf[pos] = '\0';
                                    term->twrite((unsigned char *)&event.text.text[i], 1);
                                }     
                            }
                        } else if (state == 18) {
                            if (strlen(buf) + strlen(event.text.text) >  9) {
                                state = 4;
                                pos = 0;
                            } else {
                                for (i=0;i<strlen(event.text.text);i++) {
                                    buf[pos++] = event.text.text[i];
                                    buf[pos] = '\0';
                                    term->twrite((unsigned char *)&event.text.text[i], 1);
                                }     
                            }
                        } else if (state == 9) {
							if (type == 0) {
								if (!ssh_channel_is_eof(chan) && ssh_channel_is_open(chan)) {
									sz = 1;
									i = ssh_channel_write(chan, event.text.text, strlen(event.text.text));
									if (i != strlen(event.text.text)) {
										state = 10;
									}
								}
							}
							else if (type == 1) {
								sz = 1;
								i = send(tsock, event.text.text, strlen(event.text.text), 0);
								if (i != strlen(event.text.text)) {
									state = 10;
								}
							}
                        } else if (state == 15) {
                            if (strlen(exportpath) + strlen(event.text.text) > PATH_MAX) {
                                pos = 0;
                                // do export
                                ddir->doexport(exportpath);
                                state = 0;
                                ddir->display();
                            } else {
                                for (i=0;i<strlen(event.text.text);i++) {
                                    exportpath[pos++] = event.text.text[i];
                                    exportpath[pos] = '\0';
                                    term->twrite((unsigned char *)&event.text.text[i], 1);
                                }
                            }                            
                        }
                    }
                }
            } // end switch
        } // end of message processing

        // DRAWING STARTS HERE
        SDL_Surface *screenSurf = term->getSurface();
        // clear screen
        if (fullscreen) {
            dest.x = 0;
            dest.y = 0;
            dest.w = screenWidth;
            if (toggleToolbox) {
                dest.h = 560. * scaleh;
            } else {
                dest.h = screenHeight;
            }

            SDL_RenderClear(renderer);
            if (dragging) {
                if (drag_start_x > drag_end_x) {
                    src.x = drag_end_x;
                    src.w = drag_start_x - drag_end_x;
                } else {
                    src.x = drag_start_x;
                    src.w = drag_end_x - drag_start_x;
                }
                if (drag_start_y > drag_end_y) {
                    src.y = drag_end_y;
                    src.h = drag_start_y - drag_end_y;
                } else {
                    src.y = drag_start_y;
                    src.h = drag_end_y - drag_start_y;
                }
                SDL_LockSurface(screenSurf);
                for (x = src.x; x < src.x + src.w; x++) {
                    for (y = src.y; y < src.y + src.h; y++) {
                        copypixel = getpixel(screenSurf, x, y);
                        r = 0xFF ^ ((copypixel >> 16) & 0xFF);
                        g = 0xFF ^ ((copypixel >> 8) & 0xFF);
                        b = 0xFF ^ (copypixel & 0xFF);
                        putpixel(screenSurf, x, y, (copypixel & 0xFF000000) | (r << 16) | (g << 8) | b);
                    }
                }
                SDL_UnlockSurface(screenSurf);
            }            
            screen = SDL_CreateTextureFromSurface(renderer, screenSurf);
            SDL_RenderCopy(renderer, screen, 0, &dest);

            SDL_DestroyTexture(screen);

            if (!toggleToolbox) {
                dest.x = 0;
                dest.y = 400 * scaleh;
                dest.w = 640 * scalew;
                dest.h = 80 * scaleh;

                SDL_RenderCopy(renderer, toolbox, 0, &dest);

                if (sz) {
                    dest.x = 561 * scalew;
                    dest.y = 413 * scaleh;
                    dest.w = 23 * scalew;
                    dest.h = 23 * scaleh;

                    SDL_RenderCopy(renderer, led, 0, &dest);
                }
                if (rz) {
                    dest.x = 561 * scalew;
                    dest.y = 439 * scaleh;
                    dest.w = 23 * scalew;
                    dest.h = 23 * scaleh;

                    SDL_RenderCopy(renderer, led, 0, &dest);
                }

                if (ft) {
                    dest.x = 484 * scalew;
                    dest.y = 416 * scaleh;
                    dest.w = 68 * scalew;
                    dest.h = 44 * scaleh;

                    SDL_RenderCopy(renderer, transfering, 0, &dest);
                }
                if (term->isIceEnabled()) {
                    dest.x = 437 * scalew;
                    dest.y = 429 * scaleh;
                    dest.w = 57 * scalew;
                    dest.h = 45 * scaleh;
                    SDL_RenderCopy(renderer, ice, 0, &dest);
                }
            }
            if (toggleHelp) {
                dest.x = 116 * scalew;
                dest.y = 58 * scaleh + (80 * scaleh);
                dest.w = 416 * scalew;
                dest.h = 291 * scaleh;
                SDL_RenderCopy(renderer, help, 0, &dest);
            }               
      
        } else {
            dest.x = 0;
            dest.y = 0;
            dest.w = screenWidth;
            dest.h = screenHeight;

            SDL_RenderClear(renderer);
            if (dragging) {
                if (drag_start_x > drag_end_x) {
                    src.x = drag_end_x;
                    src.w = drag_start_x - drag_end_x;
                } else {
                    src.x = drag_start_x;
                    src.w = drag_end_x - drag_start_x;
                }
                if (drag_start_y > drag_end_y) {
                    src.y = drag_end_y;
                    src.h = drag_start_y - drag_end_y;
                } else {
                    src.y = drag_start_y;
                    src.h = drag_end_y - drag_start_y;
                }
                SDL_LockSurface(screenSurf);
                for (x = src.x; x < src.x + src.w; x++) {
                    for (y = src.y; y < src.y + src.h; y++) {
                        copypixel = getpixel(screenSurf, x, y);
                        r = 0xFF ^ ((copypixel >> 16) & 0xFF);
                        g = 0xFF ^ ((copypixel >> 8) & 0xFF);
                        b = 0xFF ^ (copypixel & 0xFF);
                        putpixel(screenSurf, x, y, (copypixel & 0xFF000000) | (r << 16) | (g << 8) | b);
                    }
                }
                SDL_UnlockSurface(screenSurf);
            }            
            screen = SDL_CreateTextureFromSurface(renderer, screenSurf);
            SDL_RenderCopy(renderer, screen, 0, &dest);

            SDL_DestroyTexture(screen);

            if (!toggleToolbox) {
                dest.x = 0;
                dest.y = 400 * scaleh;
                dest.w = 640 * scalew;
                dest.h = 80 * scaleh;

                SDL_RenderCopy(renderer, toolbox, 0, &dest);

                if (sz) {
                    dest.x = 561 * scalew;
                    dest.y = 413 * scaleh;
                    dest.w = 23 * scalew;
                    dest.h = 23 * scaleh;

                    SDL_RenderCopy(renderer, led, 0, &dest);
                }
                if (rz) {
                    dest.x = 561 * scalew;
                    dest.y = 439 * scaleh;
                    dest.w = 23 * scalew;
                    dest.h = 23 * scaleh;

                    SDL_RenderCopy(renderer, led, 0, &dest);
                }

                if (ft) {
                    dest.x = 484 * scalew;
                    dest.y = 416 * scaleh;
                    dest.w = 68 * scalew;
                    dest.h = 44 * scaleh;

                    SDL_RenderCopy(renderer, transfering, 0, &dest);
                }
                if (term->isIceEnabled()) {
                    dest.x = 437 * scalew;
                    dest.y = 429 * scaleh;
                    dest.w = 57 * scalew;
                    dest.h = 45 * scaleh;
                    SDL_RenderCopy(renderer, ice, 0, &dest);
                }
            }
            if (toggleHelp) {
                dest.x = 116 * scalew;
                dest.y = 58 * scaleh;
                dest.w = 416 * scalew;
                dest.h = 291 * scaleh;
                SDL_RenderCopy(renderer, help, 0, &dest);
            }               
        }

  

        // DRAWING ENDS HERE
        rz = 0;
        sz = 0;
        // finally, update the screen :)
        SDL_RenderPresent(renderer);

        if (state == 4) {
            sess = ssh_new();
            if (sess == NULL) {
                term->twrite((unsigned char*)"Unable to create SSH Session! (PRESS ESCAPE)", 44);
                state = -1;
            }
            snprintf(knownhosts, PATH_MAX, "%sknown_hosts", SDL_GetPrefPath("Magicka", "MagiTerm"));
            ssh_options_set(sess, SSH_OPTIONS_HOST, host);
            ssh_options_set(sess, SSH_OPTIONS_PORT, &port);
            ssh_options_set(sess, SSH_OPTIONS_USER, username);
            ssh_options_set(sess, SSH_OPTIONS_KNOWNHOSTS, knownhosts);
            //         ssh_options_set(sess, SSH_OPTIONS_LOG_VERBOSITY, &verbosity);
            con_inf = (struct con_info_t*)malloc(sizeof(struct con_info_t));
            if (!con_inf) {
                state = 0;
                ddir->display();
                continue;
            }
            con_inf->connected = 0;
            con_inf->sess = sess;
            conn_thread = SDL_CreateThread(do_ssh_connect, "CONNECT", con_inf);
            state = 41;
        } else if (state == 41) {
            if (con_inf->connected == 1) {
                state = 42;
            }
        } else if (state == 42) {
            SDL_WaitThread(conn_thread, &i);
            free(con_inf);
            con_inf = NULL;
            if (i != SSH_OK) {
                term->twrite((unsigned char*)"Unable to connect! (PRESS ESCAPE)", 33);
                ssh_free(sess);
                state = -1;
            }
            else {
                state = 5;
            }
        } else if (state == 5) {
#if LIBSSH_VERSION_MINOR < 9
            sshstate = ssh_is_server_known(sess);
#else
            sshstate = ssh_session_is_known_server(sess);
#endif
#if LIBSSH_VERSION_MINOR < 8
            if (ssh_get_publickey(sess, &key) != SSH_OK) {
#else
            if (ssh_get_server_publickey(sess, &key) != SSH_OK) {
#endif
                exit(-1);
            }
            
            ssh_get_publickey_hash(key, SSH_PUBLICKEY_HASH_SHA1, &hash, &hlen);

            //hlen = ssh_get_pubkey_hash(sess, &hash);
            ssh_key_free(key);

            if (hlen == 0) {
                ssh_disconnect(sess);
                ssh_free(sess);
                state = -1;
            } else {
                switch (sshstate) {
#if LIBSSH_VERSION_MINOR < 9
                    case SSH_SERVER_KNOWN_OK:
#else
                    case SSH_KNOWN_HOSTS_OK:
#endif
                        state = 8;
                        break; /* ok */
#if LIBSSH_VERSION_MINOR < 9
                    case SSH_SERVER_KNOWN_CHANGED:
#else
                    case SSH_KNOWN_HOSTS_CHANGED:
#endif
                        hexa = ssh_get_hexa(hash, hlen);
                        term->twrite((unsigned char *)"Host key for server has changed! It is now:\r\n", 45);
                        term->twrite((unsigned char *)hexa, strlen(hexa));
                        term->twrite((unsigned char *)"\r\nFor security reasons, connection will be stopped\r\n(PRESS ESCAPE)", 66);
                        ssh_clean_pubkey_hash(&hash);
                        ssh_string_free_char(hexa);
                        ssh_disconnect(sess);
                        ssh_free(sess);
                        state = -1;
                        break;
#if LIBSSH_VERSION_MINOR < 9
                    case SSH_SERVER_FOUND_OTHER:
#else
                    case SSH_KNOWN_HOSTS_OTHER:
#endif
                        term->twrite((unsigned char *)"The host key for this server was not found but an other type of key exists.\r\n", 77);
                        term->twrite((unsigned char *)"An attacker might change the default server key to confuse your client into thinking the key does not exist\r\n(PRESS ESCAPE)", 123);
                        ssh_clean_pubkey_hash(&hash);
                        ssh_disconnect(sess);
                        ssh_free(sess);
                        state = -1;
                        break;
#if LIBSSH_VERSION_MINOR < 9
                    case SSH_SERVER_FILE_NOT_FOUND:
#else
                    case SSH_KNOWN_HOSTS_NOT_FOUND:
#endif
                        term->twrite((unsigned char *)"Could not find known host file.\r\n", 33);
                        term->twrite((unsigned char *)"If you accept the host key here, the file will be automatically created.\r\n", 74);
                        /* fallback to SSH_SERVER_NOT_KNOWN behavior */
#if LIBSSH_VERSION_MINOR < 9
                    case SSH_SERVER_NOT_KNOWN:
#else
                    case SSH_KNOWN_HOSTS_UNKNOWN:
#endif
                        hexa = ssh_get_hexa(hash, hlen);
                        term->twrite((unsigned char *)"The server is unknown. Do you trust the host key?\r\n", 51);
                        term->twrite((unsigned char *)"Public key hash: ", 17);
                        term->twrite((unsigned char *)hexa, strlen(hexa));
                        term->twrite((unsigned char *)"\r\n: ", 4);
                        ssh_string_free_char(hexa);
                        ssh_clean_pubkey_hash(&hash);
                        state = 6;
                        break;
#if LIBSSH_VERSION_MINOR < 9
                    case SSH_SERVER_ERROR:
#else
                    case SSH_KNOWN_HOSTS_ERROR:
#endif
                        term->twrite((unsigned char *)"Server Error!\r\n", 15);
                        ssh_clean_pubkey_hash(&hash);
                        ssh_disconnect(sess);
                        ssh_free(sess);
                        state = -1;
                        break;
                }
            }
        } else if (state == 7) {
            if (strcasecmp(buf, "yes") == 0) {
#if LIBSSH_VERSION_MINOR < 9
                if (ssh_write_knownhost(sess) < 0) {
#else
                if (ssh_session_update_known_hosts(sess) < 0) {
#endif
                    term->twrite((unsigned char *)"Error! (PRESS ESCAPE)\r\n", 23);
                    ssh_disconnect(sess);
                    ssh_free(sess);
                    state = -1;
                } else {
                    state = 8;
                }
            } else {
                term->twrite((unsigned char *)"Disconnected! (PRESS ESCAPE)\r\n", 30);
                ssh_disconnect(sess);
                ssh_free(sess);
                state = -1;                
            }
		}
		else if (state == 8) {
			i = ssh_userauth_password(sess, username, password);
			if (i != SSH_AUTH_SUCCESS) {
				term->twrite((unsigned char*)"Access Denied! (PRESS ESCAPE)\r\n", 31);
				ssh_disconnect(sess);
				ssh_free(sess);
				state = -1;
			}
			else {
				chan = ssh_channel_new(sess);
				if (chan == NULL) {
					term->twrite((unsigned char*)"Couldn't create channel! (PRESS ESCAPE)\r\n", 41);
					ssh_disconnect(sess);
					ssh_free(sess);
					state = -1;
				}
				else {
					i = ssh_channel_open_session(chan);
					if (i != SSH_OK) {
						term->twrite((unsigned char*)"Couldn't open session! (PRESS ESCAPE)\r\n", 39);
						ssh_channel_send_eof(chan);
						ssh_channel_close(chan);
						ssh_channel_free(chan);
						ssh_disconnect(sess);
						ssh_free(sess);
						state = -1;
					}
					else {
						i = ssh_channel_request_pty_size(chan, "ansi-bbs", 80, 25);
						if (i != SSH_OK) {
							term->twrite((unsigned char*)"Couldn't set request PTY! (PRESS ESCAPE)\r\n", 42);
							ssh_channel_send_eof(chan);
							ssh_channel_close(chan);
							ssh_channel_free(chan);
							ssh_disconnect(sess);
							ssh_free(sess);
							state = -1;
						}
						else {
							i = ssh_channel_request_shell(chan);
							if (i != SSH_OK) {
								term->twrite((unsigned char*)"Couldn't request shell! (PRESS ESCAPE)\r\n", 40);
								ssh_channel_send_eof(chan);
								ssh_channel_close(chan);
								ssh_channel_free(chan);
								ssh_disconnect(sess);
								ssh_free(sess);
								state = -1;
							}
							else {
								term->setZmodem(&zmodem);
								state = 9;
							}
						}
					}
				}
			}
        }
        else if (state == 90) {
            // telnet connecting
            con_inf = (struct con_info_t*)malloc(sizeof(struct con_info_t));
            if (!con_inf) {
                state = 0;
                ddir->display();
                continue;
            }

            con_inf->connected = 0;
            con_inf->host = host;
            con_inf->port = port;
            con_inf->sock = &tsock;
            conn_thread = SDL_CreateThread(do_telnet_connect, "CONNECT", con_inf);
            state = 91;
        }
        else if (state == 91) {
            if (con_inf->connected == 1) {
                free(con_inf);
                con_inf = NULL;
                SDL_WaitThread(conn_thread, &i);
                if (i < 0) {
                    term->twrite((unsigned char*)"An Error occured!\r\n", 19);
#ifdef _MSC_VER
                    WSACleanup();
#endif
                    state = -1;
                }
                else if (i == 0) {
                    term->twrite((unsigned char*)"Unable to connect!\r\n", 19);
#ifdef _MSC_VER
                    WSACleanup();
#endif
                    state = -1;
                }
                else {
                    topts[0] = 255;
                    topts[1] = 253;
                    topts[2] = 3;
                    send(tsock, (char*)topts, 3, 0);

                    topts[0] = 255;
                    topts[1] = 251;
                    topts[2] = 24;
                    send(tsock, (char*)topts, 3, 0);

                    term->twrite((unsigned char*)"Connected!\r\n", 12);
                    term->setZmodem(&zmodem);
                    state = 9;
                }
            }
        } else if (state == 9) {
			if (type == 0) {
				i = ssh_channel_read_nonblocking(chan, buffer, 4096, 0);
			}
			else if (type == 1) {
				i = recv_telnet(tsock, buffer, 4096, 0);
			}
            if (i > 0) {
                rz = 1;
                term->twrite((unsigned char *)buffer, i);
                if (zmodem == 1) {
                    state = 11;
                    snprintf(buffer, sizeof buffer, "\x1b[0;46;30m\x1b[8;18H+-----------------------------------------+");
                    term->twrite((unsigned char *)buffer, strlen(buffer));
                    snprintf(buffer, sizeof buffer, "\x1b[9;18H|                                         |");
                    term->twrite((unsigned char *)buffer, strlen(buffer));
                    snprintf(buffer, sizeof buffer, "\x1b[10;18H|  ZModem Status:                         |");
                    term->twrite((unsigned char *)buffer, strlen(buffer));
                    snprintf(buffer, sizeof buffer, "\x1b[11;18H|                                         |");
                    term->twrite((unsigned char *)buffer, strlen(buffer));
                    snprintf(buffer, sizeof buffer, "\x1b[12;18H|                                         |");
                    term->twrite((unsigned char *)buffer, strlen(buffer));
                    snprintf(buffer, sizeof buffer, "\x1b[13;18H|                                         |");
                    term->twrite((unsigned char *)buffer, strlen(buffer));
                    snprintf(buffer, sizeof buffer, "\x1b[14;18H|                                         |");
                    term->twrite((unsigned char *)buffer, strlen(buffer));
                    snprintf(buffer, sizeof buffer, "\x1b[15;18H|                                         |");
                    term->twrite((unsigned char *)buffer, strlen(buffer));
                    snprintf(buffer, sizeof buffer, "\x1b[16;18H|                                         |");
                    term->twrite((unsigned char *)buffer, strlen(buffer));
                    snprintf(buffer, sizeof buffer, "\x1b[17;18H+-----------------------------------------+\x1b[0m");
                    term->twrite((unsigned char *)buffer, strlen(buffer));                    
                } else if (zmodem == 2) {
                    state = 12;
                }
            } else if (type == 0 && i < 0) {
                state = 10;
			} else if (type == 1 && i == 0) {
				state = 10;
			}
        } else if (state == 10) {
            term->twrite((unsigned char *)"\r\n\r\n\r\n\x1b[0mDisconnected...(PRESS ESCAPE)\r\n", 41);
            if (zmodem != 0) {
                zmodem = 0;
                SDL_WaitThread(dlthread, NULL);
            }

			if (type == 0) {
				ssh_channel_send_eof(chan);
				ssh_channel_close(chan);
				ssh_channel_free(chan);
				ssh_disconnect(sess);
				ssh_free(sess);
			}
			else if (type == 1) {
#ifdef _MSC_VER
				WSACleanup();
#endif
			}
            state = -1;
        } else if (state == 11) {
            if (!zmodem) {
                SDL_WaitThread(dlthread, NULL);
                snprintf(buffer, sizeof buffer, "\x1b[2JTransfer Finished....\r\n");
                term->twrite((unsigned char *)buffer, strlen(buffer));
                state = 9;
            } else {
                if (zm_update == 1) {
                    switch (zm_type) {
                        case 0: // Received Byte Count
                            snprintf(buffer, sizeof buffer, "\x1b[0;46;30m\x1b[12;18H|                                         |");
                            term->twrite((unsigned char *)buffer, strlen(buffer));
                            snprintf(buffer, sizeof buffer, "\x1b[12;23H\x1b[0;46;30mReceived %d Bytes.\x1b[0m", zm_value);
                            term->twrite((unsigned char *)buffer, strlen(buffer));
                            break;
                        case 1: // Send Byte Count
                            snprintf(buffer, sizeof buffer, "\x1b[0;46;30m\x1b[12;18H|                                         |");
                            term->twrite((unsigned char *)buffer, strlen(buffer));
                            snprintf(buffer, sizeof buffer, "\x1b[12;23H\x1b[0;46;30mSent %d Bytes.\x1b[0m", zm_value);
                            term->twrite((unsigned char *)buffer, strlen(buffer));
                            break;
                        case 2: // Receive Timeout
                            snprintf(buffer, sizeof buffer, "\x1b[0;46;30m\x1b[14;18H|                                         |");
                            term->twrite((unsigned char *)buffer, strlen(buffer));
                            snprintf(buffer, sizeof buffer, "\x1b[14;23H\x1b[0;46;30mReceiver Timeout, Aborting!\x1b[0m");
                            term->twrite((unsigned char *)buffer, strlen(buffer));
                            break;
                        case 3: // Send Timeout
                            snprintf(buffer, sizeof buffer, "\x1b[0;46;30m\x1b[14;18H|                                         |");
                            term->twrite((unsigned char *)buffer, strlen(buffer));
                            snprintf(buffer, sizeof buffer, "\x1b[14;23H\x1b[0;46;30mSend Timeout (%d times).\x1b[0m", zm_value);
                            term->twrite((unsigned char *)buffer, strlen(buffer));
                            break;
                        case 4: // Remote Cancel
                            snprintf(buffer, sizeof buffer, "\x1b[0;46;30m\x1b[14;18H|                                         |");
                            term->twrite((unsigned char *)buffer, strlen(buffer));
                            snprintf(buffer, sizeof buffer, "\x1b[14;23H\x1b[0;46;30mRemote Cancelled.\x1b[0m");
                            term->twrite((unsigned char *)buffer, strlen(buffer));
                            break;
                        case 5: // Protocol Error
                            snprintf(buffer, sizeof buffer, "\x1b[0;46;30m\x1b[14;18H|                                         |");
                            term->twrite((unsigned char *)buffer, strlen(buffer));
                            snprintf(buffer, sizeof buffer, "\x1b[14;23H\x1b[0;46;30mProtocol Error (Hdr 0x%x).\x1b[0m", zm_value);
                            term->twrite((unsigned char *)buffer, strlen(buffer));
                            break;
                        case 7: // Data Error
                            snprintf(buffer, sizeof buffer, "\x1b[0;46;30m\x1b[14;18H|                                         |");
                            term->twrite((unsigned char *)buffer, strlen(buffer));
                            snprintf(buffer, sizeof buffer, "\x1b[14;23H\x1b[0;46;30mData Error (%d times).\x1b[0m", zm_value);
                            term->twrite((unsigned char *)buffer, strlen(buffer));
                            break;
                        case 8: // File Error
                            snprintf(buffer, sizeof buffer, "\x1b[0;46;30m\x1b[14;18H|                                         |");
                            term->twrite((unsigned char *)buffer, strlen(buffer));
                            snprintf(buffer, sizeof buffer, "\x1b[14;23H\x1b[0;46;30mFile Error (%d errno).\x1b[0m", zm_value);
                            term->twrite((unsigned char *)buffer, strlen(buffer));
                            break;
                    }
                    
                    snprintf(buffer, sizeof buffer, "\x1b[19;1H");
                    term->twrite((unsigned char *)buffer, strlen(buffer));
                    zm_update = 0;
                }
            }
        } else if (state == 12) {
            // get upload filename....
            fdir = new filechooser(term);
            fdir->load();
            fdir->draw();
            state = 14;
        }
        SDL_Delay(10);
    } // end main loop
    
    save_settings();

    // free loaded bitmap
    SDL_DestroyTexture(toolbox);
    SDL_DestroyTexture(led);
    SDL_DestroyTexture(transfering);
    SDL_FreeSurface(icon);
    delete ddir;
    delete term;
    // all is well ;)
    printf("Exited cleanly\n");
    return 0;
}
