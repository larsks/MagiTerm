#include <limits.h>
#include "dialdirectory.h"
#include "mtermans.h"
#include "inih/ini.h"

#ifdef _MSC_VER
#include <wincred.h>
#define strcasecmp stricmp
#define PATH_MAX MAX_PATH
#else
#ifdef __APPLE__
#include <CoreFoundation/CoreFoundation.h>
#include <Security/Security.h>

template <typename T>
struct Releaser {
    explicit Releaser( const T& v ) : value( v ) {}
    ~Releaser() {
        CFRelease( value );
    }
    const T value;
};

#else
#ifdef __HAIKU__

// nothing

#else
#include <libsecret/secret.h>

const SecretSchema *magiterm_get_schema (void) {
    static const SecretSchema the_schema = {
        "com.magickabbs.magiterm.Password", SECRET_SCHEMA_NONE,
        {
            {  "bbsname", SECRET_SCHEMA_ATTRIBUTE_STRING },
            {  "username", SECRET_SCHEMA_ATTRIBUTE_STRING },
            {  NULL, (SecretSchemaAttributeType) 0 },
        }
    };
    return &the_schema;
}

#define MAGITERM_SCHEMA  magiterm_get_schema ()
#endif
#endif
#endif



struct dialinfo **entries;
int entry_count;

dialdir::dialdir(Terminal *term) {
    t = term;
    cur_item = 0;
    entry_count = 0;
    state = 0;
    top = 0;
    memset(searchdata, 0, 256);
}

static int handler(void* user, const char* section, const char* name,
                   const char* value)
{
    int i;

    for (i=0;i<entry_count;i++) {
        if (strcmp(section, entries[i]->name) == 0) {
            if (strcasecmp(name, "address") == 0) {
                if (entries[i]->address != NULL) {
                    free(entries[i]->address);
                }
                entries[i]->address = strdup(value);
            } else if (strcasecmp(name, "port") == 0) {
                entries[i]->port = atoi(value);
            } else if (strcasecmp(name, "username") == 0) {
                entries[i]->username = strdup(value);
			}
			else if (strcasecmp(name, "conn type") == 0) {
				entries[i]->type = atoi(value);
			}

            return 1;
        }
    }

    if (entry_count == 0) {
        entries = (struct dialinfo **)malloc(sizeof(struct dialinfo *));
    } else {
        entries = (struct dialinfo **)realloc(entries, sizeof(struct dialinfo *) * (entry_count + 1));
    }
    if (!entries) {
        exit(-1);
    }

    entries[entry_count] = (struct dialinfo *)malloc(sizeof(struct dialinfo));

    if (!entries[entry_count]) {
        exit(-1);
    }

    entries[entry_count]->name = strdup(section);
    entries[entry_count]->username = NULL;
    entries[entry_count]->address = NULL;
    entries[entry_count]->port = 22;
	entries[entry_count]->type = 0;
    if (strcasecmp(name, "address") == 0) {
        entries[entry_count]->address = strdup(value);
    } else if (strcasecmp(name, "port") == 0) {
        entries[entry_count]->port = atoi(value);
    } else if (strcasecmp(name, "username") == 0) {
        entries[entry_count]->username = strdup(value);
	}
	else if (strcasecmp(name, "conn type") == 0) {
		entries[entry_count]->type = atoi(value);
	}
    entry_count++;
    return 1;
}

void dialdir::savepass(int entry, char *username, char *password) {
	if (entry == -1) {
		entry = cur_item;
	}
    if (entries[entry]->username == NULL) {
        entries[entry]->username = strdup(username);
        save();
    }
    storepassword(entry, username, password);
}

void dialdir::storepassword(int entry, char *username, char *password) {
#ifdef _MSC_VER
    DWORD cbCreds = 1 + strlen(password);
    char target[512];
    CREDENTIAL cred = {0};

    snprintf(target, 512, "MagiTerm/%s", entries[entry]->name);

    cred.Type = CRED_TYPE_GENERIC;
    cred.TargetName = (LPSTR)target;
    cred.CredentialBlobSize = cbCreds;
    cred.CredentialBlob = (LPBYTE) password;
    cred.Persist = CRED_PERSIST_LOCAL_MACHINE;
    cred.UserName = (LPSTR)username;
    ::CredWrite (&cred, 0);
#else
#ifdef __APPLE__
    char target[512];
    snprintf(target, 512, "MagiTerm/%s", entries[entry]->name);

    const OSStatus ret = SecKeychainAddGenericPassword(NULL, strlen(target), target, strlen(username), username, strlen(password), password, NULL);
    
    Releaser<CFStringRef> str (SecCopyErrorMessageString(ret, 0));
    printf("%s\n", CFStringGetCStringPtr(str.value, kCFStringEncodingUTF8));
    
#else
#ifdef __HAIKU__
// Nothing yet
#else
    char secret_title[512];

    snprintf(secret_title, 512, "MagiTerm/%s", entries[entry]->name);

    secret_password_store_sync(MAGITERM_SCHEMA, SECRET_COLLECTION_DEFAULT, secret_title, password, NULL, NULL, "bbsname", entries[entry]->name,  "username", username, nullptr);
#endif
#endif
#endif
}

char *dialdir::getpass(int item) {
    char *pass;

	if (item == -1) {
		item = cur_item;
	}

	if (entries[item]->username == NULL) {
		return NULL;
	}

    if (retrievepassword(item, entries[item]->username, &pass) == 0) {
        return pass;
    } else {
        return NULL;
    }
}

int dialdir::retrievepassword(int entry, char *username, char **password) {
#ifdef _MSC_VER
    PCREDENTIAL pcred;
    char target[512];
    snprintf(target, 512, "MagiTerm/%s", entries[entry]->name);
    BOOL ok = ::CredRead ((LPCSTR)target, CRED_TYPE_GENERIC, 0, &pcred);
    if (!ok) {
        return 1;
    }
    *password = (char *)malloc(pcred->CredentialBlobSize + 1);
    memset(*password, 0, pcred->CredentialBlobSize + 1);
    memcpy(*password, (char *)pcred->CredentialBlob, pcred->CredentialBlobSize);
    ::CredFree (pcred);
    return 0;
#else
#ifdef __APPLE__
    char target[512];
    Uint32 len;
    char *data;
    snprintf(target, 512, "MagiTerm/%s", entries[entry]->name);
    const OSStatus ret = SecKeychainFindGenericPassword( NULL, // default keychain
                                                         strlen(target),
                                                         target,
                                                         strlen(username),
                                                         username,
                                                         &len,
                                                         (void **)&data,
                                                         0 );

    if (ret == noErr) {
        *password = (char *)malloc(len + 1);
        memset(*password, 0, len + 1);
        memcpy(*password, data, len);
        SecKeychainItemFreeContent(NULL, data);
        return 0;
    } else {
        return 1;
    }
#else
#ifdef __HAIKU__
	return 1;
#else
    GError *error = NULL;

    /* The attributes used to lookup the password should conform to the schema. */
    gchar *spassword = secret_password_lookup_sync (MAGITERM_SCHEMA, NULL, &error, "bbsname", entries[entry]->name, "username", username, nullptr);

    if (error != NULL) {
        /* ... handle the failure here */
        g_error_free (error);

    } else if (spassword == NULL) {
        /* password will be null, if no matching password found */
        return 1;
    } else {
        /* ... do something with the password */
        *password = strdup(spassword);
        secret_password_free (spassword);
        return 0;
    }
#endif
#endif
#endif
    return 1;
}

void dialdir::wipepassword(int entry, char *username) {
#ifdef _MSC_VER
    char target[512];
    snprintf(target, 512, "MagiTerm/%s", entries[entry]->name);
    ::CredDelete((LPCSTR)target, CRED_TYPE_GENERIC, 0);
#else
#ifdef __APPLE__
    char target[512];
    int len;
    char *data;
    SecKeychainItemRef ref;
    snprintf(target, 512, "MagiTerm/%s", entries[entry]->name);
    SecKeychainFindGenericPassword( NULL, // default keychain
                                                         strlen(target),
                                                         target,
                                                         strlen(username),
                                                         username,
                                                         NULL,
                                                         NULL,
                                                         &ref );
    const Releaser<SecKeychainItemRef> releaser( ref );

    SecKeychainItemDelete( ref );

#else
#ifdef __HAIKU__
// nothing yet
#else
    GError *error = NULL;
    secret_password_clear_sync (MAGITERM_SCHEMA, NULL, &error, "bbsname", entries[entry]->name, "username", username, nullptr);
    if (error != NULL) {
        g_error_free(error);
    }
#endif
#endif
#endif
}


void dialdir::doimport(char *importpath) {
    ini_parse(importpath, handler, NULL);
    sort();
    save();
}

void dialdir::load() {
    char buffer[PATH_MAX];
    char *prefpath = SDL_GetPrefPath("Magicka", "MagiTerm");
    snprintf(buffer, PATH_MAX, "%sdialdirectory.ini", prefpath);

    ini_parse(buffer, handler, NULL);
    SDL_free(prefpath);
    sort();
}

void dialdir::doexport(char *exportpath) {
    FILE *fptr;
    int i;

    fptr = fopen(exportpath, "w");

    if (fptr) {
        for (i=0;i<entry_count;i++) {
            fprintf(fptr, "[%s]\n", entries[i]->name);
            fprintf(fptr, "address = %s\n", entries[i]->address);
            fprintf(fptr, "port = %d\n", entries[i]->port);
			fprintf(fptr, "conn type = %d\n\n", entries[i]->type);
        }
        fclose(fptr);
    }
}

static int ddirCompare (const void * a, const void * b) {
    return strcmp ((*(const struct dialinfo **)a)->name, (*(const struct dialinfo **) b)->name);
}

void dialdir::sort() {
    qsort(entries, entry_count, sizeof(struct dialinfo *), ddirCompare);
}

void dialdir::save() {
    char buffer[PATH_MAX];
    char *prefpath = SDL_GetPrefPath("Magicka", "MagiTerm");
    FILE *fptr;
    int i;

    snprintf(buffer, PATH_MAX, "%sdialdirectory.ini", prefpath);

    fptr = fopen(buffer, "w");

    if (fptr) {
        for (i=0;i<entry_count;i++) {
            fprintf(fptr, "[%s]\n", entries[i]->name);
            fprintf(fptr, "address = %s\n", entries[i]->address);
            fprintf(fptr, "port = %d\n", entries[i]->port);
			fprintf(fptr, "conn type = %d\n", entries[i]->type);
            if (entries[i]->username) {
                fprintf(fptr, "username = %s\n\n", entries[i]->username);
			}
			else {
				fprintf(fptr, "\n");
			}
        }
        fclose(fptr);
    }
    SDL_free(prefpath);
}

void dialdir::display() {
    int i;
    char buffer[256];
	char *ptr;
    t->twrite((unsigned char *)"\x1b[2J\x1b[1;1H\x1b[0;0 D", 17);
    t->twrite((unsigned char *)MAGITERM_ANSI_DATA, sizeof(MAGITERM_ANSI_DATA));

    snprintf(buffer, 256, "\x1b[13;0H\x1b[0;46;30mName                                    Address\x1b[K");
    t->twrite((unsigned char *)buffer, strlen(buffer));

    snprintf(buffer, 256, "\x1b[25;0H\x1b[0;46;30mPress Ctrl-I to Add, DEL to Delete, Ctrl-E to Edit or Enter to Connect\x1b[K");
    t->twrite((unsigned char *)buffer, strlen(buffer));

    if (cur_item > top + 10) {
        top = cur_item - 10;
    } else if (cur_item < top) {
        top = cur_item;
    }

    if (entry_count == 0) {
        t->twrite((unsigned char *)"\x1b[16;35HNo Entries Found", 24);
    } else {
        t->twrite((unsigned char *)"\x1b[13;1H", 7);
        for (i=top;i<entry_count && i < top + 11;i++) {
			ptr = getpass(i);
            if (i == cur_item) {
                snprintf(buffer, 256, "\x1b[%d;1H%s\x1b[1;33m%c \x1b[0;47;30m%-35.35s %32.32s %d\x1b[K", i - top + 13, (entries[i]->type == 0 ? "\x1b[1;47;32m S" : "\x1b[1;47;31m T"), (ptr != NULL ? '*' : ' '), entries[i]->name, entries[i]->address, entries[i]->port);
            } else {
                snprintf(buffer, 256, "\x1b[%d;1H%s\x1b[1;33m%c \x1b[0m%-35.35s %32.32s %d\x1b[K", i - top + 13, (entries[i]->type == 0 ? "\x1b[1;40;32m S" : "\x1b[1;40;31m T"), (ptr != NULL ? '*' : ' '), entries[i]->name, entries[i]->address, entries[i]->port);
            }
			free(ptr);
            t->twrite((unsigned char *)buffer, strlen(buffer));            
        }
    }
}

int dialdir::enter() {
    char buffer[256];
    int i;
    if (state == 0) {
        if (entry_count > 0) {
            return 1;
        } else {
            return 0;
        }
    } else if (state == 1) {
        state = 2;
        updateCursor();
/*
        if (strlen(tmpbbs) > 0) {
            for (i=0;i<entry_count;i++) {
                if (strcmp(tmpbbs, entries[i]->name) == 0) {
                    return 0;
                }
            }
            snprintf(buffer, 256, "\x1b[0;47;30m\x1b[15;24H");
            t->twrite((unsigned char *)buffer, strlen(buffer));
            state = 2;
        }
*/
    } else if (state == 2) {
        state = 3;
        updateCursor();
//        if (strlen(tmphost) > 0) {
//            snprintf(buffer, 256, "\x1b[0;47;30m\x1b[17;24H%d", tmpport);
//            t->twrite((unsigned char *)buffer, strlen(buffer));
//            state = 3;
//        }
    } else if (state == 3) {
        state = 4;
        updateCursor();
    } else if (state == 4) {
        state = 5;
        updateCursor();
    } else if (state == 5) {
        state = 6;
        updateCursor();
    } else if (state == 6) {
        state = 7;
        updateCursor();
    } else if (state == 7) {
        state = 8;
        updateCursor();
    } else if (state == 8) {
        snprintf(buffer, 256, "\x1b[0;46;30m\x1b[12;32H                     \x1b[0;46;30m\x1b[14;32H                     ");
        t->twrite((unsigned char *)buffer, strlen(buffer));
        if (strlen(tmpbbs) == 0) {
            snprintf(buffer, 256, "\x1b[0;46;31m\x1b[12;32HCan not be empty!");
            t->twrite((unsigned char *)buffer, strlen(buffer));
            state = 1;
            updateCursor();
            return 0;
        }
        if (strlen(tmphost) == 0) {
            snprintf(buffer, 256, "\x1b[0;46;31m\x1b[14;32HCan not be empty!");
            t->twrite((unsigned char *)buffer, strlen(buffer));
            state = 2;
            updateCursor();
            return 0;
        }
        if (tmpport == 0) {
            if (tmptype == 0) {
                tmpport = 22;
            } else if (tmptype == 1) {
                tmpport = 23;
            }
        }
        if (editing == -1) {
            for (i=0;i<entry_count;i++) {
                if (strcasecmp(tmpbbs, entries[i]->name) == 0) {
                    snprintf(buffer, 256, "\x1b[0;46;31m\x1b[12;32HDuplicate Entry!");
                    t->twrite((unsigned char *)buffer, strlen(buffer));
                    state = 1;
                    updateCursor();
                    return 0;
                }
            }
            if (entry_count == 0) {
                entries = (struct dialinfo **)malloc(sizeof(struct dialinfo *));
            } else {
                entries = (struct dialinfo **)realloc(entries, sizeof(struct dialinfo *) * (entry_count + 1));
            }
            if (!entries) {
                exit(-1);
            }

            entries[entry_count] = (struct dialinfo *)malloc(sizeof(struct dialinfo));
            if (!entries[entry_count]) exit(-1);
            entries[entry_count]->name = strdup(tmpbbs);
            entries[entry_count]->address = strdup(tmphost);
            entries[entry_count]->port = tmpport;
			if (strlen(tmpuser) > 0) {
				entries[entry_count]->username = strdup(tmpuser);
			}
			else {
				entries[entry_count]->username = NULL;
			}
			entries[entry_count]->type = tmptype;
            entry_count++;
			if (entries[entry_count - 1]->username != NULL && strlen(tmppass) > 0) {
				savepass(entry_count - 1, entries[entry_count - 1]->username, tmppass);
			}
        } else {
            if (entries[editing]->username) {
                wipepassword(editing, entries[editing]->username);
                free(entries[editing]->username);
                entries[editing]->username = NULL;
            }

            free(entries[editing]->name);
            free(entries[editing]->address);
            entries[editing]->name = strdup(tmpbbs);
            entries[editing]->address = strdup(tmphost);
            entries[editing]->port = tmpport;
			entries[editing]->type = tmptype;
			if (strlen(tmpuser) > 0) {
				entries[editing]->username = strdup(tmpuser);
				if (strlen(tmppass) > 0) {
					savepass(editing, entries[editing]->username, tmppass);
				}
			}
        }
        sort();
        save();

        display();
        state = 0;        
    }
    return 0;
}

void dialdir::backspace() {
    char buffer[256];    
	char* ptr;
	int i;

    if (state == 1) {
        if (strlen(tmpbbs) > 0) {
            tmpbbs[strlen(tmpbbs)-1] = '\0';
            if (strlen(tmpbbs) >= 31) {
               snprintf(buffer, 256, "\x1b[0;47;30m\x1b[13;23H<%s\x1b[s \x1b[u", &tmpbbs[strlen(tmpbbs)-31]);
            } else {
               snprintf(buffer, 256, "\x1b[0;47;30m\x1b[13;23H %s\x1b[s \x1b[u", tmpbbs);
            }
             t->twrite((unsigned char *)buffer, strlen(buffer));
        }
    } else if (state == 2) {
        if (strlen(tmphost) > 0) {
            tmphost[strlen(tmphost)-1] = '\0';
            if (strlen(tmphost) >= 31) {
                snprintf(buffer, 256, "\x1b[0;47;30m\x1b[15;23H<%s\x1b[s \x1b[u", &tmphost[strlen(tmphost)-31]);
            } else {        
                snprintf(buffer, 256, "\x1b[0;47;30m\x1b[15;23H %s\x1b[s \x1b[u", tmphost);
            }            
            t->twrite((unsigned char *)buffer, strlen(buffer));
        }    
    } else if (state == 3) {
        tmpport = tmpport / 10;
        if (tmpport == 0) {
            snprintf(buffer, 256, "\x1b[0;47;30m\x1b[17;24H\x1b[s \x1b[u");
        } else {
            snprintf(buffer, 256, "\x1b[0;47;30m\x1b[17;24H%d\x1b[s \x1b[u", tmpport);
        }
        t->twrite((unsigned char *)buffer, strlen(buffer));        
	}
	else if (state == 6) {
		if (strlen(tmpuser) > 0) {
			tmpuser[strlen(tmpuser) - 1] = '\0';
			if (strlen(tmpuser) >= 31) {
				snprintf(buffer, 256, "\x1b[0;47;30m\x1b[20;23H<%s\x1b[s \x1b[u", &tmpuser[strlen(tmpuser) - 31]);
			}
			else {
				snprintf(buffer, 256, "\x1b[0;47;30m\x1b[20;23H %s\x1b[s \x1b[u", tmpuser);
			}
			t->twrite((unsigned char*)buffer, strlen(buffer));
		}
	}
	else if (state == 7) {
		if (strlen(tmppass) > 0) {
			tmppass[strlen(tmppass) - 1] = '\0';
			if (strlen(tmppass) >= 31) {
				snprintf(buffer, 256, "\x1b[0;47;30m\x1b[22;23H<");
				ptr = &buffer[strlen(buffer)];
				for (i = 31; i < strlen(tmppass); i++) {
					*ptr++ = '*';
					*ptr = '\0';
				}
				strcat(buffer, "\x1b[s \x1b[u");
			}
			else {
				snprintf(buffer, 256, "\x1b[0;47;30m\x1b[22;23H ");
				ptr = &buffer[strlen(buffer)];
				for (i = 0; i < strlen(tmppass); i++) {
					*ptr++ = '*';
					*ptr = '\0';
				}
				strcat(buffer, "\x1b[s \x1b[u");
			}
			t->twrite((unsigned char*)buffer, strlen(buffer));
		}
	}
}

void dialdir::process(char *data) {
    char buffer[256];
	char* ptr;
    int i;
    if (state == 0) {
		strncat(searchdata, data, 256);
		for (i = 0;i< entry_count;i++) {
#ifdef _MSC_VER
			if (_strnicmp(searchdata, entries[i]->name, (strlen(searchdata) > 256 ? 256 : strlen(searchdata))) == 0) {
#else
			if (strncasecmp(searchdata, entries[i]->name, (strlen(searchdata) > 256 ? 256 : strlen(searchdata))) == 0) {
#endif
				cur_item = i;
				display();
				break;
			}
		}
	} else if (state == 1) {
        if (strlen(tmpbbs) + strlen(data) > 200) {
            return;
        }
        strcat(tmpbbs, data);
        if (strlen(tmpbbs) >= 31) {
            snprintf(buffer, 256, "\x1b[0;47;30m\x1b[13;23H<%s", &tmpbbs[strlen(tmpbbs)-31]);
        } else {
            snprintf(buffer, 256, "\x1b[0;47;30m\x1b[13;23H %s", tmpbbs);
        }
        t->twrite((unsigned char *)buffer, strlen(buffer));
    } else if (state == 2) {
        if (strlen(tmphost) + strlen(data) > 200) {
            return;
        }        
        strcat(tmphost, data);
        if (strlen(tmphost) >= 31) {
            snprintf(buffer, 256, "\x1b[0;47;30m\x1b[15;23H<%s", &tmphost[strlen(tmphost)-31]);
        } else {        
            snprintf(buffer, 256, "\x1b[0;47;30m\x1b[15;23H %s", tmphost);
        }
        t->twrite((unsigned char *)buffer, strlen(buffer));
    } else if (state == 3) {
        for (i=0;i<strlen(data);i++) {
            tmpport = tmpport * 10 + (data[i] - '0');
        }
        if (tmpport == 0) {
            snprintf(buffer, 256, "\x1b[0;47;30m\x1b[17;23H ");
        } else {
            snprintf(buffer, 256, "\x1b[0;47;30m\x1b[17;23H %d", tmpport);
        }   
        t->twrite((unsigned char *)buffer, strlen(buffer));             
	}
	else if (state == 4) {
		if (data[0] == ' ') {
			tmptype = 0;
			snprintf(buffer, 256, "\x1b[18;20H\x1b[0;46;30m|  [%c] SSH  [%c] Telnet                 |\x1b[18;24H", (tmptype == 0 ? 'X' : ' '), (tmptype == 1 ? 'X' : ' '));
			t->twrite((unsigned char*)buffer, strlen(buffer));
		}

	}
	else if (state == 5) {
		if (data[0] == ' ') {
			tmptype = 1;
			snprintf(buffer, 256, "\x1b[18;20H\x1b[0;46;30m|  [%c] SSH  [%c] Telnet                 |\x1b[18;33H", (tmptype == 0 ? 'X' : ' '), (tmptype == 1 ? 'X' : ' '));
			t->twrite((unsigned char*)buffer, strlen(buffer));
		}
	}
	else if (state == 6) {
		if (strlen(tmpuser) + strlen(data) > 200) {
			return;
		}
		strcat(tmpuser, data);
		if (strlen(tmpuser) >= 31) {
			snprintf(buffer, 256, "\x1b[0;47;30m\x1b[20;23H<%s", &tmpuser[strlen(tmpuser) - 31]);
		}
		else {
			snprintf(buffer, 256, "\x1b[0;47;30m\x1b[20;23H %s", tmpuser);
		}
		t->twrite((unsigned char*)buffer, strlen(buffer));
	}
	else if (state == 7) {
		if (strlen(tmppass) + strlen(data) > 200) {
			return;
		}
		strcat(tmppass, data);
		if (strlen(tmppass) >= 31) {
			snprintf(buffer, 256, "\x1b[0;47;30m\x1b[22;23H<");
			ptr = &buffer[strlen(buffer)];
			for (i = 31; i < strlen(tmppass); i++) {
				*ptr++ = '*';
				*ptr = '\0';
			}
		}
		else {
			snprintf(buffer, 256, "\x1b[0;47;30m\x1b[22;23H ");
			ptr = &buffer[strlen(buffer)];
			for (i = 0; i < strlen(tmppass); i++) {
				*ptr++ = '*';
				*ptr = '\0';
			}
		}
		t->twrite((unsigned char*)buffer, strlen(buffer));
	}
}

void dialdir::pgup() {
    if (state == 0) {
		searchdata[0] = '\0';
        if (entry_count == 0) {
            return;
        }
        cur_item -= 11;
        if (cur_item < 0) {
            cur_item = 0;
        }
        display();
    }
}

void dialdir::up() {
    if (state == 0) {
		searchdata[0] = '\0';
        if (entry_count == 0) {
            return;
        }
        cur_item--;
        if (cur_item < 0) {
            cur_item = entry_count - 1;
        }
        display();
    } else if (state > 0) {
        state = state - 1;
        if (state == 0) {
            state = 8;
        }
        updateCursor();
    }
}

void dialdir::down() {
    if (state == 0) {
		searchdata[0] = '\0';
        if (entry_count == 0) {
            return;
        }
        cur_item++;
        if (cur_item == entry_count) {
            cur_item = 0;
        }
        display();
    } else if (state < 9) {
        state = state + 1;
        if (state == 9) {
            state = 1;
        }
        updateCursor();
    }
}


void dialdir::pgdown() {
    if (state == 0) {
		searchdata[0] = '\0';
        if (entry_count == 0) {
            return;
        }
        cur_item += 11;
        if (cur_item >= entry_count) {
            cur_item = entry_count -1;
        }
        display();
    }   
}

void dialdir::tab() {
    if (state > 0 && state < 9) {
        state = state + 1;
        if (state == 9) {
            state = 1;
        }
        updateCursor();
    }
}

void dialdir::edit() {
    char buffer[256];
	char* ptr;
	int i;

    if (entry_count == 0 || state != 0) {
        return;
    }

    editing = cur_item;
	tmptype = entries[editing]->type;
    snprintf(buffer, 256, "\x1b[11;20H\x1b[0;46;30m+--------------------------------------+");
    t->twrite((unsigned char *)buffer, strlen(buffer));
    snprintf(buffer, 256, "\x1b[12;20H\x1b[0;46;30m| BBS Name:                            |");
    t->twrite((unsigned char *)buffer, strlen(buffer));
    snprintf(buffer, 256, "\x1b[13;20H\x1b[0;46;30m|  \x1b[0;47m                                  \x1b[0;46;30m  |");
    t->twrite((unsigned char *)buffer, strlen(buffer));
    snprintf(buffer, 256, "\x1b[14;20H\x1b[0;46;30m| Hostname:                            |");
    t->twrite((unsigned char *)buffer, strlen(buffer));
    snprintf(buffer, 256, "\x1b[15;20H\x1b[0;46;30m|  \x1b[0;47m                                  \x1b[0;46;30m  |");
    t->twrite((unsigned char *)buffer, strlen(buffer));
    snprintf(buffer, 256, "\x1b[16;20H\x1b[0;46;30m| Port:                                |");
    t->twrite((unsigned char *)buffer, strlen(buffer));
    snprintf(buffer, 256, "\x1b[17;20H\x1b[0;46;30m|  \x1b[0;47m                                  \x1b[0;46;30m  |");
    t->twrite((unsigned char *)buffer, strlen(buffer));
	snprintf(buffer, 256, "\x1b[18;20H\x1b[0;46;30m|  [%c] SSH  [%c] Telnet                 |", (tmptype == 0 ? 'X' : ' '), (tmptype == 1 ? 'X' : ' '));
	t->twrite((unsigned char*)buffer, strlen(buffer));
	snprintf(buffer, 256, "\x1b[19;20H\x1b[0;46;30m| Username:                            |");
	t->twrite((unsigned char*)buffer, strlen(buffer));
	snprintf(buffer, 256, "\x1b[20;20H\x1b[0;46;30m|  \x1b[0;47m                                  \x1b[0;46;30m  |");
	t->twrite((unsigned char*)buffer, strlen(buffer));
	snprintf(buffer, 256, "\x1b[21;20H\x1b[0;46;30m| Password:                            |");
	t->twrite((unsigned char*)buffer, strlen(buffer));
	snprintf(buffer, 256, "\x1b[22;20H\x1b[0;46;30m|  \x1b[0;47m                                  \x1b[0;46;30m  |");
	t->twrite((unsigned char*)buffer, strlen(buffer));
    snprintf(buffer, 256, "\x1b[23;20H\x1b[0;46;30m+---------------------------[ OK ]-----+");
    t->twrite((unsigned char *)buffer, strlen(buffer));
    snprintf(buffer, 256, "\x1b[0;47;30m\x1b[17;24H");
    t->twrite((unsigned char *)buffer, strlen(buffer));
    strcpy(tmpbbs, entries[editing]->name);
    strcpy(tmphost, entries[editing]->address);
    tmpport = entries[editing]->port;
	if (entries[editing]->username != NULL) {
		strcpy(tmpuser, entries[editing]->username);
	}
	else {
		memset(tmpuser, 0, 200);
	}

	ptr = getpass(editing);

	if (ptr != NULL) {
		strcpy(tmppass, ptr);
		free(ptr);
	}
	else {
		memset(tmppass, 0, 200);
	}

    if (strlen(tmpbbs) >= 31) {
        snprintf(buffer, 256, "\x1b[0;47;30m\x1b[13;23H<%s", &tmpbbs[strlen(tmpbbs)-31]);
    } else {
        snprintf(buffer, 256, "\x1b[0;47;30m\x1b[13;23H %s", tmpbbs);
    }
    t->twrite((unsigned char *)buffer, strlen(buffer));
    if (strlen(tmphost) >= 31) {
        snprintf(buffer, 256, "\x1b[0;47;30m\x1b[15;23H<%s", &tmphost[strlen(tmphost)-31]);
    } else {        
        snprintf(buffer, 256, "\x1b[0;47;30m\x1b[15;23H %s", tmphost);
    }
    t->twrite((unsigned char *)buffer, strlen(buffer));
    if (tmpport == 0) {
        snprintf(buffer, 256, "\x1b[0;47;30m\x1b[17;23H ");
    } else {
        snprintf(buffer, 256, "\x1b[0;47;30m\x1b[17;23H %d", tmpport);
    }
	t->twrite((unsigned char*)buffer, strlen(buffer));

	if (strlen(tmpuser) >= 31) {
		snprintf(buffer, 256, "\x1b[0;47;30m\x1b[20;23H<%s", &tmpuser[strlen(tmpuser) - 31]);
	}
	else if (strlen(tmpuser) > 0) {
		snprintf(buffer, 256, "\x1b[0;47;30m\x1b[20;23H %s", tmpuser);
	}
	t->twrite((unsigned char*)buffer, strlen(buffer));

	if (strlen(tmppass) >= 31) {
		snprintf(buffer, 256, "\x1b[0;47;30m\x1b[22;23H<");
		ptr = &buffer[strlen(buffer)];
		for (i = 0; i < strlen(tmppass); i++) {
			*ptr++ = '*';
			*ptr = '\0';
		}
	}
	else {
		snprintf(buffer, 256, "\x1b[0;47;30m\x1b[22;23H ");
		ptr = &buffer[strlen(buffer)];
		for (i = 0; i < strlen(tmppass); i++) {
			*ptr++ = '*';
			*ptr = '\0';
		}
	}
	t->twrite((unsigned char*)buffer, strlen(buffer));
    state = 1;
    updateCursor();
}

void dialdir::updateCursor() {
    char buffer[30];
    int plen;

	if (state == 8) {
		snprintf(buffer, 30, "\x1b[23;48H\x1b[0;40;37m[ OK ]\x1b[0;46;37m");
	}
	else {
		snprintf(buffer, 30, "\x1b[23;48H\x1b[0;46;30m[ OK ]\x1b[0;46;37m");
	}
	t->twrite((unsigned char*)buffer, strlen(buffer));
    if (state == 1) {
        if (strlen(tmpbbs) >= 31) {
            snprintf(buffer, 10, "\x1b[13;55H");
        } else {
            snprintf(buffer, 10, "\x1b[13;%dH", strlen(tmpbbs) + 24);
        }
    } else if (state == 2) {
        if (strlen(tmphost) >= 31) {
            snprintf(buffer, 10, "\x1b[15;55H");
        } else {
            snprintf(buffer, 10, "\x1b[15;%dH", strlen(tmphost) + 24);
        }
    } else if (state == 3) {
        if (tmpport > 0) {
            snprintf(buffer, 10, "%d", tmpport);
            plen = strlen(buffer);
            snprintf(buffer, 10, "\x1b[17;%dH", plen + 24);
        } else {
            snprintf(buffer, 10, "\x1b[17;24H");
        }
	}
	else if (state == 4) {
		snprintf(buffer, 10, "\x1b[18;24H");
	}
	else if (state == 5) {
		snprintf(buffer, 10, "\x1b[18;33H");
	} 
	else if (state == 6) {
		if (strlen(tmpuser) >= 31) {
			snprintf(buffer, 10, "\x1b[20;55H");
		}
		else {
			snprintf(buffer, 10, "\x1b[20;%dH", strlen(tmpuser) + 24);
		}
	}
	else if (state == 7) {
		if (strlen(tmppass) >= 31) {
			snprintf(buffer, 10, "\x1b[22;55H");
		}
		else {
			snprintf(buffer, 10, "\x1b[22;%dH", strlen(tmppass) + 24);
		}
	}
	else if (state == 8) {
		snprintf(buffer, 10, "\x1b[23;52H");
	}

    t->twrite((unsigned char *)buffer, strlen(buffer));
}

void dialdir::insert() {
    char buffer[256];
	if (state != 0) {
		return;
	}
    editing = -1;
	snprintf(buffer, 256, "\x1b[11;20H\x1b[0;46;30m+--------------------------------------+");
	t->twrite((unsigned char*)buffer, strlen(buffer));
    snprintf(buffer, 256, "\x1b[12;20H\x1b[0;46;30m| BBS Name:                            |");
    t->twrite((unsigned char *)buffer, strlen(buffer));
    snprintf(buffer, 256, "\x1b[13;20H\x1b[0;46;30m|  \x1b[0;47m                                  \x1b[0;46;30m  |");
    t->twrite((unsigned char *)buffer, strlen(buffer));
    snprintf(buffer, 256, "\x1b[14;20H\x1b[0;46;30m| Hostname:                            |");
    t->twrite((unsigned char *)buffer, strlen(buffer));
    snprintf(buffer, 256, "\x1b[15;20H\x1b[0;46;30m|  \x1b[0;47m                                  \x1b[0;46;30m  |");
    t->twrite((unsigned char *)buffer, strlen(buffer));
    snprintf(buffer, 256, "\x1b[16;20H\x1b[0;46;30m| Port:                                |");
    t->twrite((unsigned char *)buffer, strlen(buffer));
    snprintf(buffer, 256, "\x1b[17;20H\x1b[0;46;30m|  \x1b[0;47;30m 22                               \x1b[0;46;30m  |");
    t->twrite((unsigned char *)buffer, strlen(buffer));
	snprintf(buffer, 256, "\x1b[18;20H\x1b[0;46;30m|  [X] SSH  [ ] Telnet                 |");
	t->twrite((unsigned char*)buffer, strlen(buffer));
	snprintf(buffer, 256, "\x1b[19;20H\x1b[0;46;30m| Username:                            |");
	t->twrite((unsigned char*)buffer, strlen(buffer));
	snprintf(buffer, 256, "\x1b[20;20H\x1b[0;46;30m|  \x1b[0;47m                                  \x1b[0;46;30m  |");
	t->twrite((unsigned char*)buffer, strlen(buffer));
	snprintf(buffer, 256, "\x1b[21;20H\x1b[0;46;30m| Password:                            |");
	t->twrite((unsigned char*)buffer, strlen(buffer));
	snprintf(buffer, 256, "\x1b[22;20H\x1b[0;46;30m|  \x1b[0;47m                                  \x1b[0;46;30m  |");
	t->twrite((unsigned char*)buffer, strlen(buffer));
    snprintf(buffer, 256, "\x1b[23;20H\x1b[0;46;30m+---------------------------[ OK ]-----+");
    t->twrite((unsigned char *)buffer, strlen(buffer));
    snprintf(buffer, 256, "\x1b[0;47;30m\x1b[13;24H");
    t->twrite((unsigned char *)buffer, strlen(buffer));
    memset(tmpbbs, 0, 200); 
    memset(tmphost, 0, 200);
    memset(tmpuser, 0, 200);
    memset(tmppass, 0, 200);
    tmpport = 22;   
    state = 1;
	tmptype = 0;
}


char *dialdir::getname() {
    return entries[cur_item]->name;
}

char *dialdir::gethost() {
    return entries[cur_item]->address;
}

int dialdir::getport() {
    return entries[cur_item]->port;
}

char *dialdir::getuser() {
    return entries[cur_item]->username;
}

int dialdir::gettype() {
	return entries[cur_item]->type;
}

void dialdir::escape() {
	if (state == 0) {
		searchdata[0] = '\0';
	} else {
		state = 0;
		display();
	}
}

void dialdir::remove() {
    int i;
    if (entry_count == 0) {
        return;
    }

    if (entries[cur_item]->username) {
        wipepassword(cur_item, entries[cur_item]->username);
        free(entries[cur_item]->username);
    }
    free(entries[cur_item]->name);
    free(entries[cur_item]->address);
    free(entries[cur_item]);

    for (i=cur_item;i<entry_count-1;i++) {
        entries[i] = entries[i+1];
    }

    entry_count--;
    if (entry_count == 0) {
        free(entries);
		entries = NULL;
    } else {
        entries = (struct dialinfo **)realloc(entries, sizeof(struct dialinfo *) * entry_count);
    }
    cur_item--;
    if (cur_item < 0) {
        cur_item = 0;
    }
    save();
    display();
}

dialdir::~dialdir() {
    int i;

    for (i=0;i<entry_count;i++) {
        free(entries[i]->name);
        free(entries[i]->address);
        if (entries[i]->username) {
            free(entries[i]->username);
        }
        free(entries[i]);
    }
    free(entries);
}
