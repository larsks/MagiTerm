#include <string>
#include <sstream>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <regex>
#ifndef _MSC_VER
#include <pwd.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <libgen.h>
#if defined(__linux__)
#include <pty.h>
#include <utmp.h>
#else
#if defined(__FreeBSD__)
#include <sys/types.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <libutil.h>
#endif
#if defined(__OpenBSD__)
#include <sys/types.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <util.h>
#endif
#endif
#include <termios.h>
#include <signal.h>
#ifndef __APPLE__
#include <unistd.h>
#else
#include <CoreFoundation/CoreFoundation.h>
extern "C" {
    extern void NSBeep();
}
#include <util.h>
#include <sys/ioctl.h>
#include <libgen.h>
#endif
#ifdef __sun
#include <sys/file.h>
#include <sys/ioctl.h>
#include <stropts.h>
#include <unistd.h>
#include <libgen.h>
#endif
#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>
#else
#include <SDL.h>
#define WIN32_LEAN_AND_MEAN 1
#include <Shlobj.h>
#include <shellapi.h>
#include <direct.h>
#include <vector>
#include <string>
#define PATH_MAX MAX_PATH
#endif
#include "Terminal.h"
#include "fonts/cp437.h"
#include "fonts/potnoodle.h"
#include "fonts/topazplus.h"
#include "fonts/mkplus.h"
#include "fonts/microknight.h"
#include "fonts/topaz.h"
#include "fonts/mosoul.h"
extern "C" {
	#include "Xmodem/zmodem.h"
}
#define IAC 255
#define IAC_WILL 251
#define IAC_WONT 252
#define IAC_DO 253
#define IAC_DONT 254
#define IAC_TRANSMIT_BINARY 0
#define IAC_SUPPRESS_GO_AHEAD 3
#define IAC_ECHO 1

#define IS 0
#define SEND 1
#define REPLY 2
#define NAME 3

#define TERMINAL_TYPE 24
#define NAWS 31
#define TERMINAL_SPEED 32

#ifdef _MSC_VER
#define PATH_SEP '\\'
#else
#define PATH_SEP '/'
#endif

extern int type;
extern int tsock;
extern ssh_channel chan;
extern SDL_Thread *dlthread;

extern int rz;
extern int sz;
extern int ft;

int echo = 0;

struct zmodem_info {
    int *ptr;
    char *uploadfile;
};

struct zmodem_info zm_inf;

extern SDL_Surface *loadImageSurf(unsigned char *iaddr, int ilen);

int do_zmodem_download(void *ptr);
int do_zmodem_upload(void *ptr);


int send_telnet(int sock, const char* buff, int len, int flags) {
	int i;
	for (i = 0; i < len; i++) {
		if ((unsigned char)buff[i] == IAC) {
			send(sock, &buff[i], 1, flags);
		}
		send(sock, &buff[i], 1, flags);
	}
	return i;
}

static int gotIAC = 0;
static int gotTermType = 0;
static int dosendterm = 0;
static unsigned char iacc;

int recv_telnet(int sock, char *buff, int len, int flags) {
	int i;
	unsigned char c;
	unsigned char buf[15];
	int count;

	i = 0;

	while (i < len) {
		count = recv(sock, (char *)&c, 1, flags);

		if (count < 0) {
			if (i > 0) {
				return i;
			}
			return count;
		}
		else if (count == 0) {
			return 0;
		}
		if (c == IAC) {
			if (gotIAC == 1) {
				buff[i++] = c;
			}
			else if(gotIAC != 3) {
				gotIAC = 1;
			}
		}
		else {
			if (gotIAC != 0) {
				if (gotIAC == 1) {
					if (c == 250) {
						gotIAC = 3;
					}
					else {
						if (c == IAC_WILL || c == IAC_WONT || c == IAC_DO || c == IAC_DONT) {
							iacc = c;
							gotIAC = 2;
						}
						else {
							gotIAC = 0;
						}
					}
				}
				else if (gotIAC == 2) {

					if (iacc == IAC_WILL) {
						buf[0] = IAC;
						if (c == IAC_ECHO || c == IAC_SUPPRESS_GO_AHEAD || c == IAC_TRANSMIT_BINARY || c == TERMINAL_TYPE || c == NAWS) {
							buf[1] = IAC_DO;
						}
						else {
							buf[1] = IAC_DONT;
						}
						buf[2] = c;
						send(sock, (char*)buf, 3, 0);
					} else if (iacc == IAC_WONT) {
						buf[0] = IAC;
						buf[1] = IAC_DONT;
						buf[2] = c;
						send(sock, (char*)buf, 3, 0);
					} else if (iacc == IAC_DO) {
						if (c == NAWS) {
							buf[0] = IAC;
							buf[1] = 250;
							buf[2] = NAWS;
							buf[3] = 0;
							buf[4] = 80;
							buf[5] = 0;
							buf[6] = 24;
							buf[7] = IAC;
							buf[8] = 240;
							send(sock, (char*)buf, 9, 0);
						}
						else {
							buf[0] = IAC;
							buf[1] = IAC_WILL;
							buf[2] = c;
							send(sock, (char*)buf, 3, 0);
						}
					}
					else if (iacc == IAC_DONT) {
						buf[0] = IAC;
						buf[1] = IAC_WONT;
						buf[2] = c;
						send(sock, (char*)buf, 3, 0);
					}

					gotIAC = 0;
				}
				else if (gotIAC == 3) {
	
					if (c == TERMINAL_TYPE) {
						gotTermType = 1;
					}
					else {

					}

					if (c == SEND) {
						if (gotTermType == 1) {
							dosendterm = 1;
							gotTermType = 0;
						}
					}

					if (c == 240) {
						if (dosendterm == 1) {
							buf[0] = IAC;
							buf[1] = 250;
							buf[2] = TERMINAL_TYPE;
							buf[3] = IS;
							buf[4] = 'A';
							buf[5] = 'N';
							buf[6] = 'S';
							buf[7] = 'I';
							buf[8] = IAC;
							buf[9] = 240;
							send(sock, (char *)buf, 10, 0);
							dosendterm = 0;
						}
						gotIAC = 0;
					}
				}
			}
			else {
				buff[i++] = c;
			}
		}
	}

	return i;
}

#ifdef __sun
int openpty(int *amaster, int *aslave, char *name, void *termp, void *winp) {
	int ptm;
	char *pname;
	int pts;

	ptm = open("/dev/ptmx", O_RDWR);
	
	grantpt(ptm);
	unlockpt(ptm);

	pname = ptsname(ptm);
	
	if (name != NULL) {
		strcpy(name, pname);
	}	

	pts = open(pname, O_RDWR);
	ioctl(pts, I_PUSH, "ptem");
	ioctl(pts, I_PUSH, "ldterm");
	ioctl(pts, I_PUSH, "ttcompat");

        if (termp != NULL) {
                tcsetattr(pts, TCSAFLUSH, (const termios *)termp);
        }
        if (winp != NULL) {
                ioctl(pts, TIOCSWINSZ, winp);
        }


	*amaster = ptm;
	*aslave = pts;
	
	return 0;
}
#endif

void zmodem_upload(char *filename) {
	unsigned char buf[3];
	
	if (zm_inf.uploadfile != NULL) {
        free(zm_inf.uploadfile);
    }

    zm_inf.uploadfile = strdup(filename);


	if (type == 1) {
		buf[0] = IAC;
		buf[1] = IAC_WILL;
		buf[2] = IAC_TRANSMIT_BINARY;
		send(tsock, (char *)buf, 3, 0);
	}

    dlthread = SDL_CreateThread(do_zmodem_upload, "ZMODEM", &zm_inf);
}


#ifndef _MSC_VER
int ttySetRaw(int fd, struct termios *prevTermios) {
    struct termios t;

    if (tcgetattr(fd, &t) == -1)
        return -1;

    if (prevTermios != NULL)
        *prevTermios = t;

    t.c_lflag &= ~(ICANON | ISIG | IEXTEN | ECHO);
    t.c_iflag &= ~(BRKINT | ICRNL | IGNBRK | IGNCR | INLCR | INPCK | ISTRIP | IXON | PARMRK);
    t.c_oflag &= ~OPOST;
    t.c_cc[VMIN] = 1;
    t.c_cc[VTIME] = 0;

    if (tcsetattr(fd, TCSAFLUSH, &t) == -1)
        return -1;

    return 0;
}
#endif
Terminal::Terminal(SDL_Window *window)
{
    //ctor
    Uint32 rmask, gmask, bmask, amask;
    int i, j;

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    rmask = 0xff000000;
    gmask = 0x00ff0000;
    bmask = 0x0000ff00;
    amask = 0x000000ff;
#else
    rmask = 0x000000ff;
    gmask = 0x0000ff00;
    bmask = 0x00ff0000;
    amask = 0xff000000;
#endif

    this->win = window;
    this->screen = SDL_CreateRGBSurface(SDL_SWSURFACE, 640, 480, 32, rmask, gmask, bmask, amask);
    this->screenbacker = SDL_CreateRGBSurface(SDL_SWSURFACE, 640, 400, 32, rmask, gmask, bmask, amask);
    this->screencopy = SDL_CreateRGBSurface(SDL_SWSURFACE, 640, 400, 32, rmask, gmask, bmask, amask);
    this->blink_screen = SDL_CreateRGBSurface(SDL_SWSURFACE, 640, 400, 32, rmask, gmask, bmask, amask);
    this->scrollback_screen = SDL_CreateRGBSurface(SDL_SWSURFACE, 640, 400, 32, rmask, gmask, bmask, amask);
    this->font_bmp = loadImageSurf((unsigned char *)CP437_IMAGE_DATA, sizeof(CP437_IMAGE_DATA));
    this->pot_noodle_bmp = loadImageSurf((unsigned char *)POTNOODLE_IMAGE_DATA, sizeof(POTNOODLE_IMAGE_DATA));
    this->topaz_plus_bmp = loadImageSurf((unsigned char *)TOPAZPLUS_IMAGE_DATA, sizeof(TOPAZPLUS_IMAGE_DATA));
    this->mk_plus_bmp = loadImageSurf((unsigned char *)MKPLUS_IMAGE_DATA, sizeof(MKPLUS_IMAGE_DATA));
    this->mk_bmp = loadImageSurf((unsigned char *)MK_IMAGE_DATA, sizeof(MK_IMAGE_DATA));
    this->topaz_bmp = loadImageSurf((unsigned char *)TOPAZ_IMAGE_DATA, sizeof(TOPAZ_IMAGE_DATA));    
    this->mosoul_bmp = loadImageSurf((unsigned char *)MOSOUL_IMAGE_DATA, sizeof(MOSOUL_IMAGE_DATA));   
    this->color = 7;
    this->bgcolor = 0;
    this->cur_col = 0;
    this->cur_row = 0;
    this->state = 0;
    this->bold = 0;
    this->save_col = 0;
    this->save_row = 0;
    this->vt320_count = 0;
    this->cursor = 1;
    this->zmodem_detect = 0;
    this->zmodem = NULL;
    this->curfont = 0;
    this->blinking = 0;
    this->last_show = 0;
    this->blink_toggle = 0;
    this->high_intensity_bg = 0;
    this->high_intensity_bg_enabled = 0;
    this->blink_disabled = 0;
    this->scrollingBack = 0;
    this->scrollBackTop = 75;
    this->icecolours = 0;
	this->param_count = 0;

	chan = NULL;

    for (i=0;i<80;i++) {
        for (j=0;j<25;j++) {
            screen_buffer[i][j] = ' ';
        }
        for (j=0;j<100;j++) {
            scrollback_buffer[i][j] = ' ';
        }        
    }

}

void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel) {
    int bpp = surface->format->BytesPerPixel;
    /* Here p is the address to the pixel we want to set */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
    case 1:
        *p = pixel;
        break;

    case 2:
        *(Uint16 *)p = pixel;
        break;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
            p[0] = (pixel >> 16) & 0xff;
            p[1] = (pixel >> 8) & 0xff;
            p[2] = pixel & 0xff;
        } else {
            p[0] = pixel & 0xff;
            p[1] = (pixel >> 8) & 0xff;
            p[2] = (pixel >> 16) & 0xff;
        }
        break;

    case 4:
        *(Uint32 *)p = pixel;
        break;
    }
}

Uint32 getpixel(SDL_Surface *surface, int x, int y) {
    int bpp = surface->format->BytesPerPixel;
    /* Here p is the address to the pixel we want to retrieve */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
    case 1:
        return *p;
        break;

    case 2:
        return *(Uint16 *)p;
        break;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            return p[0] << 16 | p[1] << 8 | p[2];
        else
            return p[0] | p[1] << 8 | p[2] << 16;
        break;

    case 4:
        return *(Uint32 *)p;
        break;

    default:
        return 0;       /* shouldn't happen, but avoids warnings */
    }
}

void Terminal::clickLink(int x, int y) {
    std::regex urlr("(((news|(ht|f)tp(s?))\\://){1}\\S+)");
    std::stringstream ss;
    std::smatch match;
    std::string str;
    int i;
#ifndef _MSC_VER
    const char *path = getenv("PATH");
    char *path_copy = strdup(path);
    char *ptr2;
    struct stat s;
    int found = 0;
    char filename[PATH_MAX];
    pid_t pid;



#endif

 
    for (i=0;i<80;i++) {
        if (scrollingBack) {
            ss << scrollback_buffer[i][scrollBackTop + y];
        } else {
            ss << screen_buffer[i][y];
        }
    }
    
    str = ss.str();
    std::regex_search(str, match, urlr);

    for (i=0;i<match.size();i++) {
         if (x >= match.position(i) && x <= match.position(i) + match.length(i)) {
            // got a match
            
#ifdef _MSC_VER
            ShellExecute(NULL, "open", match[i].str().c_str(), NULL, NULL, SW_SHOWNORMAL);
#else

            ptr2 = strtok(path_copy, ":");
            while (ptr2 != NULL) {
#ifdef __APPLE__
                snprintf(filename, PATH_MAX, "%s/open", ptr2);
#else                
                snprintf(filename, PATH_MAX, "%s/xdg-open", ptr2);
#endif
                if (stat(filename, &s) == 0) {
                    found = 1;
                    break;
                }
                ptr2 = strtok(NULL, ":");
            }

            if (found) {
                pid = fork();
                if (pid == 0) {
                    execlp(filename, filename, match[i].str().c_str(), NULL);
                    exit(-1);
                } else if (pid < 0) {
                    printf("error forking...\r\n");
                } 
            }
            free(path_copy);
#endif            
            return;
        }
    }
}

void Terminal::scrollback() {
    int j, x, y, i;
    SDL_Rect src, dest;    
    scrollingBack = !scrollingBack;
    if (scrollingBack) {
        scrollBackTop = 75;
        SDL_FillRect(scrollback_screen, 0, 0xff000000);     
        for (i=scrollBackTop;i<scrollBackTop + 25;i++) {
            for (j = 0;j<80;j++) {
                src.x = (scrollback_buffer[j][i] % 32) * 8;
                src.y = (scrollback_buffer[j][i] / 32) * 16;
                src.w = 8;
                src.h = 16;

                dest.x = j * 8;
                dest.y = (i - scrollBackTop) * 16;
                dest.w = 8;
                dest.h = 16; 
                SDL_BlitSurface(font_bmp, &src, scrollback_screen, &dest);
                SDL_LockSurface(scrollback_screen);
                for (x = dest.x; x< dest.x + dest.w; x++) {
                    for (y = dest.y; y < dest.y + dest.h; y++) {
                        if (getpixel(scrollback_screen, x, y) == 0xff000000) {
                            putpixel(scrollback_screen, x, y, colours[7]);
                        } else {
                            putpixel(scrollback_screen, x, y, colours[0]);
                        }
                    }
                }
                SDL_UnlockSurface(scrollback_screen);
            }
        }   
    }
}

void Terminal::scrollUp() {
    SDL_Rect src, dest;
    int i, j, x, y;
    src.x = 0;
    src.y = 16;
    src.w = 640;
    src.h = 384;
    dest.x = 0; 
    dest.y = 0;
    dest.w = 640;
    dest.h = 384;
    SDL_FillRect(screencopy, 0, 0xff000000);
    SDL_BlitSurface(screenbacker, &src, screencopy, &dest);
    src.x = 0;
    src.y = 0;
    src.w = 640;
    src.h = 400;
    dest.x = 0; 
    dest.y = 0;
    dest.w = 640;
    dest.h = 400;   
    SDL_FillRect(screenbacker, 0, 0xff000000);     
    SDL_BlitSurface(screencopy, &src, screenbacker, &dest);
    src.x = 0;
    src.y = 16;
    src.w = 640;
    src.h = 384;
    dest.x = 0; 
    dest.y = 0;
    dest.w = 640;
    dest.h = 384;
    SDL_FillRect(screencopy, 0, 0xff000000);
    SDL_BlitSurface(blink_screen, &src, screencopy, &dest);
    src.x = 0;
    src.y = 0;
    src.w = 640;
    src.h = 400;
    dest.x = 0; 
    dest.y = 0;
    dest.w = 640;
    dest.h = 400;   
    SDL_FillRect(blink_screen, 0, 0xff000000);     
    SDL_BlitSurface(screencopy, &src, blink_screen, &dest);    
    for (i=0;i<99;i++) {
        for (j=0;j<80;j++) {
            scrollback_buffer[j][i] = scrollback_buffer[j][i+1];
        }
    }

    for (j=0;j<80;j++) {
        scrollback_buffer[j][99] = screen_buffer[j][0];
    }

    src.x = 0;
    src.y = 16;
    src.w = 640;
    src.h = 384;
    dest.x = 0; 
    dest.y = 0;
    dest.w = 640;
    dest.h = 384;
    SDL_FillRect(screencopy, 0, 0xff000000);
    SDL_BlitSurface(scrollback_screen, &src, screencopy, &dest);
    src.x = 0;
    src.y = 0;
    src.w = 640;
    src.h = 400;
    dest.x = 0; 
    dest.y = 0;
    dest.w = 640;
    dest.h = 400;   
    SDL_FillRect(scrollback_screen, 0, 0xff000000);     
    SDL_BlitSurface(screencopy, &src, scrollback_screen, &dest);    


    for (j = 0;j<80;j++) {
        src.x = (scrollback_buffer[j][scrollBackTop + 24] % 32) * 8;
        src.y = (scrollback_buffer[j][scrollBackTop + 24] / 32) * 16;
        src.w = 8;
        src.h = 16;

        dest.x = j * 8;
        dest.y = (scrollBackTop + 24) * 16;
        dest.w = 8;
        dest.h = 16; 
        SDL_BlitSurface(font_bmp, &src, scrollback_screen, &dest);
        SDL_LockSurface(scrollback_screen);
        for (x = dest.x; x< dest.x + dest.w; x++) {
            for (y = dest.y; y < dest.y + dest.h; y++) {
                if (getpixel(scrollback_screen, x, y) == 0xff000000) {
                    putpixel(scrollback_screen, x, y, colours[7]);
                } else {
                    putpixel(scrollback_screen, x, y, colours[0]);
                }
            }
        }
        SDL_UnlockSurface(scrollback_screen);
    }

    for (i=0;i<24;i++) {
        for (j=0;j<80;j++) {
            screen_buffer[j][i] = screen_buffer[j][i+1];
        }
    }
    for (j=0;j<80;j++) {
        screen_buffer[j][24] = ' ';
    }
}

char *Terminal::gettext(int sx, int sy, int ex, int ey) {
    int cols = ex - sx;
    int rows = ey - sy;
    int x, y;

    char *data = (char *)malloc(rows * (cols + 1) + 1);
    if (!data) {
        return NULL;
    }
    memset(data, 0, rows * (cols + 1) + 1);
    for (y = sy; y < ey; y ++) {
        for (x = sx; x < ex; x ++) {
            if (scrollingBack) {
                data[(y - sy) * (cols + 1) + (x - sx)] = scrollback_buffer[x][scrollBackTop + y];
            } else {
                data[(y - sy) * (cols + 1) + (x - sx)] = screen_buffer[x][y];
            }
        }
        if (rows > 1) {
            data[(y - sy) * (cols + 1) + cols] = '\n';
        }
    }
    return data;
}

void Terminal::drawChar(unsigned char c) {
    int x;
    int y;
    SDL_Rect src, dest;

    screen_buffer[cur_col][cur_row] = c;

    src.x = (c % 32) * 8;
    src.y = (c / 32) * 16;
    src.w = 8;
    src.h = 16;

    dest.x = cur_col * 8;
    dest.y = cur_row * 16;
    dest.w = 8;
    dest.h = 16;

    switch(curfont) {
        case 0:
            SDL_BlitSurface(font_bmp, &src, screenbacker, &dest);
            break;
        case 1:
            SDL_BlitSurface(pot_noodle_bmp, &src, screenbacker, &dest);           
            break;
        case 2:
            SDL_BlitSurface(topaz_plus_bmp, &src, screenbacker, &dest);             
            break;
        case 3:
            SDL_BlitSurface(mk_plus_bmp, &src, screenbacker, &dest);             
            break;
        case 4:
            SDL_BlitSurface(mk_bmp, &src, screenbacker, &dest);             
            break;
        case 5:
            SDL_BlitSurface(topaz_bmp, &src, screenbacker, &dest);            
            break;
        case 6:
            SDL_BlitSurface(mosoul_bmp, &src, screenbacker, &dest);        
            break;
    }
    

    SDL_LockSurface(screenbacker);
    SDL_LockSurface(blink_screen);

    for (x = dest.x; x< dest.x + dest.w; x++) {
        for (y = dest.y; y < dest.y + dest.h; y++) {
            if (!blinking) {
                if (getpixel(screenbacker, x, y) == 0xff000000) {
                    putpixel(screenbacker, x, y, colours[color]);
                    putpixel(blink_screen, x, y, colours[color]);
                } else {
                    putpixel(screenbacker, x, y, colours[bgcolor]);
                    putpixel(blink_screen, x, y, colours[bgcolor]);
                }
            } else {
                if (getpixel(screenbacker, x, y) == 0xff000000) {
                    putpixel(screenbacker, x, y, colours[color]);
                } else {
                    putpixel(screenbacker, x, y, colours[bgcolor]);
                } 
                putpixel(blink_screen, x, y, colours[bgcolor]);
            }
        }
    }

    SDL_UnlockSurface(screenbacker);
    SDL_UnlockSurface(blink_screen);
    cur_col++;
    if (cur_col == 80) {
        cur_col = 0;
        cur_row++;
    }
    if (cur_row >= 25) {
        // scroll_up();
        scrollUp();
        cur_row = 24;
    }
}

void Terminal::sb_up() {
    int j, x, y, i;
    SDL_Rect src, dest;
    if (scrollBackTop == 0) {
        return;
    }
    scrollBackTop--;

    SDL_FillRect(scrollback_screen, 0, 0xff000000);       
    for (i=scrollBackTop;i<scrollBackTop + 25;i++) {
        for (j = 0;j<80;j++) {
            src.x = (scrollback_buffer[j][i] % 32) * 8;
            src.y = (scrollback_buffer[j][i] / 32) * 16;
            src.w = 8;
            src.h = 16;

            dest.x = j * 8;
            dest.y = (i - scrollBackTop) * 16;
            dest.w = 8;
            dest.h = 16; 
            SDL_BlitSurface(font_bmp, &src, scrollback_screen, &dest);
            SDL_LockSurface(scrollback_screen);
            for (x = dest.x; x< dest.x + dest.w; x++) {
                for (y = dest.y; y < dest.y + dest.h; y++) {
                    if (getpixel(scrollback_screen, x, y) == 0xff000000) {
                        putpixel(scrollback_screen, x, y, colours[7]);
                    } else {
                        putpixel(scrollback_screen, x, y, colours[0]);
                    }
                }
            }
            SDL_UnlockSurface(scrollback_screen);
        }
    }
}

void Terminal::sb_down() {
    int j, x, y, i;
    SDL_Rect src, dest;    
    if (scrollBackTop == 75) {
        return;
    }
    scrollBackTop++;
    SDL_FillRect(scrollback_screen, 0, 0xff000000);     
    for (i=scrollBackTop;i<scrollBackTop + 25;i++) {
        for (j = 0;j<80;j++) {
            src.x = (scrollback_buffer[j][i] % 32) * 8;
            src.y = (scrollback_buffer[j][i] / 32) * 16;
            src.w = 8;
            src.h = 16;

            dest.x = j * 8;
            dest.y = (i - scrollBackTop) * 16;
            dest.w = 8;
            dest.h = 16; 
            SDL_BlitSurface(font_bmp, &src, scrollback_screen, &dest);
            SDL_LockSurface(scrollback_screen);
            for (x = dest.x; x< dest.x + dest.w; x++) {
                for (y = dest.y; y < dest.y + dest.h; y++) {
                    if (getpixel(scrollback_screen, x, y) == 0xff000000) {
                        putpixel(scrollback_screen, x, y, colours[7]);
                    } else {
                        putpixel(scrollback_screen, x, y, colours[0]);
                    }
                }
            }
            SDL_UnlockSurface(scrollback_screen);
        }
    }    
}

void Terminal::twrite(unsigned char *c, int len)
{
    int i, j, k;
    SDL_Rect dest;
    char buf[10];

    for (i=0;i<len;i++) {
        if (state == 0) {
            if (c[i] == 0x1b) {
                state = 1;
            } else {
                if (c[i] == '\r') {
                    cur_col=0;
                } else if (c[i] == '\n') {
                    cur_row++;
                    if (cur_row == 25) {
                        scrollUp();
                        cur_row--;
                    }
                } else if (c[i] == 7) {
                    // beep
#if defined(_MSC_VER)
                    MessageBeep(0);
#elif defined(__APPLE__)
                    NSBeep();
#elif defined(SDL_VIDEO_DRIVER_X11)
                    SDL_SysWMinfo info;
                    SDL_VERSION(&info.version);
                    SDL_GetWindowWMInfo(win, &info);
                    XBell(info.info.x11.display, 100);
#endif
                } else if (c[i] == 8) {
                    cur_col--;
                    if (cur_col < 0) {
                        cur_col = 0;
                    }
                } else if (c[i] == 12) {
					SDL_FillRect(screenbacker, 0, bgcolor);
                    SDL_FillRect(blink_screen, 0, bgcolor);
                    for (j=0;j<75;j++) {
                        for (k=0;k<80;k++) {
                            scrollback_buffer[k][j] = scrollback_buffer[k][j+25];
                        }
                    }
                    for (j=0;j<25;j++) {
                        for (k=0;k<80;k++) {
                            scrollback_buffer[k][j+75] = screen_buffer[k][j];
                            screen_buffer[k][j] = ' ';
                                
                        }
                    }
					cur_col = 0;
					cur_row = 0;                    
                } else {
                    if (c[i] == 24 && zmodem_detect == 0) {
                        zmodem_detect = 1;
                    } else if (c[i] == 'B' && zmodem_detect == 1) {
                        zmodem_detect = 2;
                    } else if (c[i] == '0' && zmodem_detect == 2) {
                        zmodem_detect = 3;
                    } else if (c[i] == '0' && zmodem_detect == 3) {
                        if (zmodem != NULL) {

                            *zmodem = 1;
                            zm_inf.ptr = zmodem;
                            dlthread = SDL_CreateThread(do_zmodem_download, "ZMODEM", &zm_inf);
                        }
                        zmodem_detect = 0;
                        return;
                    } else if (c[i] == '1' && zmodem_detect == 3) {
						if (zmodem != NULL) {
							*zmodem = 2;
							zm_inf.ptr = zmodem;
						}
                        zmodem_detect = 0;
                        return;                       
                    } else {
                        zmodem_detect = 0;
                    }
                    drawChar(c[i]);
                }
            }
        } else if (state == 1) {
            if (c[i] == '[') {
                state = 2;
                continue;
            } else {
                state = 0;
                //printf("Not CSI %d\n", c[i]);
                continue;
            }
        } else if (state == 2) {
            param_count = 0;
            for (j=0;j<16;j++) {
                params[j] = 0;
            }
            state = 3;
        }

        if (state == 3) {
            if (c[i] == ';') {
				if (param_count < 15) {
					param_count++;
				}
				continue;
			} else if (c[i] >= '0' && c[i] <= '9') {
				if (!param_count) param_count = 1;
				params[param_count-1] = params[param_count-1] * 10 + (c[i] - '0');
				continue;
			} else {
				state = 4;
			}
        }

        if (state == 4) {
            switch (c[i]) {
                case 'H':
                case 'f':
                    if (params[0]) params[0]--;
                    if (params[1]) params[1]--;

					cur_col = params[1];
					cur_row = params[0];
                    if (cur_col > 79) {
                        cur_col = 79;
                    }

                    if (cur_row > 24) {
                        cur_row = 24;
                    }


                    state = 0;
                    break;
                case 'A':
                    if (param_count > 0) {
                        cur_row -= params[0];
                    } else {
                        cur_row--;
                    }

                    if (cur_row < 0) {
                        cur_row = 0;
                    }
                    state = 0;
                    break;
                case 'B':
                    if (param_count > 0) {
                        cur_row += params[0];
                    } else {
                        cur_row++;
                    }

                    if (cur_row > 24) {
                        cur_row = 24;
                    }
                    state = 0;
                    break;
				case 'C':
					if (param_count > 0) {
                        cur_col += params[0];
					} else {
                        cur_col++;
					}

                    if (cur_col > 79) {
                        cur_col = 79;
                    }
					state = 0;
					break;
            	case 'D':
					if (param_count > 0) {
						cur_col -= params[0];
					} else {
						cur_col--;
					}

                    if (cur_col < 0) {
                        cur_col = 0;
                    }

					state = 0;
					break;
                case 's':
                    save_col = cur_col;
                    save_row = cur_row;

                    state = 0;
                    break;
                case 'u':
                    cur_col = save_col;
                    cur_row = save_row;

                    state = 0;
                    break;
                case 'm':
                    for (j=0;j<param_count;j++) {
                        switch (params[j]) {
                            case 0:
                                color = 7;
                                bgcolor = 0;
                                bold = 0;
                                blinking = 0;
                                if (high_intensity_bg) {
                                    switch(bgcolor) {
                                        case 8:
                                            bgcolor = 0;
                                            break;
                                        case 12:
                                            bgcolor = 4;
                                            break;
                                        case 10:
                                            bgcolor = 2;
                                            break;
                                        case 14:
                                            bgcolor = 6;
                                            break;
                                        case 9:
                                            bgcolor = 1;
                                            break;
                                        case 13:
                                            bgcolor = 5;
                                            break;
                                        case 11:
                                            bgcolor = 3;
                                            break;
                                        case 15:
                                            bgcolor = 7;
                                            break;
                                        default:
                                            break;
                                    }
                                    high_intensity_bg_enabled = 0;
                                }
                                break;
                            case 1:
                                bold = 1;
                                blinking = 0;
                                {
                                    switch(color) {
                                        case 0:
                                            color = 8;
                                            break;
                                        case 4:
                                            color = 12;
                                            break;
                                        case 2:
                                            color = 10;
                                            break;
                                        case 6:
                                            color = 14;
                                            break;
                                        case 1:
                                            color = 9;
                                            break;
                                        case 5:
                                            color = 13;
                                            break;
                                        case 3:
                                            color = 11;
                                            break;
                                        case 7:
                                            color = 15;
                                            break;
                                        default:
                                            break;
                                    }
                                    
                                }
                                if (high_intensity_bg) {
                                    switch(bgcolor) {
                                        case 8:
                                            bgcolor = 0;
                                            break;
                                        case 12:
                                            bgcolor = 4;
                                            break;
                                        case 10:
                                            bgcolor = 2;
                                            break;
                                        case 14:
                                            bgcolor = 6;
                                            break;
                                        case 9:
                                            bgcolor = 1;
                                            break;
                                        case 13:
                                            bgcolor = 5;
                                            break;
                                        case 11:
                                            bgcolor = 3;
                                            break;
                                        case 15:
                                            bgcolor = 7;
                                            break;
                                        default:
                                            break;
                                    }
                                    high_intensity_bg_enabled = 0;
                                }                                
                                break;
                            case 2:
                                bold = 0;
                                blinking = 0;
                                {
                                    switch(color) {
                                        case 8:
                                            color = 0;
                                            break;
                                        case 12:
                                            color = 4;
                                            break;
                                        case 10:
                                            color = 2;
                                            break;
                                        case 14:
                                            color = 6;
                                            break;
                                        case 9:
                                            color = 1;
                                            break;
                                        case 13:
                                            color = 5;
                                            break;
                                        case 11:
                                            color = 3;
                                            break;
                                        case 15:
                                            color = 7;
                                            break;
                                        default:
                                            break;
                                    }
                                    
                                    
                                }
                                if (high_intensity_bg) {
                                    switch(bgcolor) {
                                        case 8:
                                            bgcolor = 0;
                                            break;
                                        case 12:
                                            bgcolor = 4;
                                            break;
                                        case 10:
                                            bgcolor = 2;
                                            break;
                                        case 14:
                                            bgcolor = 6;
                                            break;
                                        case 9:
                                            bgcolor = 1;
                                            break;
                                        case 13:
                                            bgcolor = 5;
                                            break;
                                        case 11:
                                            bgcolor = 3;
                                            break;
                                        case 15:
                                            bgcolor = 7;
                                            break;
                                        default:
                                            break;
                                    }
                                    high_intensity_bg_enabled = 0;
                                }                                
                                break;
                            case 6:
                            case 5:
                                if (high_intensity_bg) {
                                    switch(bgcolor) {
                                        case 0:
                                            bgcolor = 8;
                                            break;
                                        case 4:
                                            bgcolor = 12;
                                            break;
                                        case 2:
                                            bgcolor = 10;
                                            break;
                                        case 6:
                                            bgcolor = 14;
                                            break;
                                        case 1:
                                            bgcolor = 9;
                                            break;
                                        case 5:
                                            bgcolor = 13;
                                            break;
                                        case 3:
                                            bgcolor = 11;
                                            break;
                                        case 7:
                                            bgcolor = 15;
                                            break;
                                        default:
                                            break;
                                    }
                                    high_intensity_bg_enabled = 1;
                                }
                                if (!blink_disabled) {
                                    blinking = 1;
                                }
                                {
                                    switch(color) {
                                        case 8:
                                            color = 0;
                                            break;
                                        case 12:
                                            color = 4;
                                            break;
                                        case 10:
                                            color = 2;
                                            break;
                                        case 14:
                                            color = 6;
                                            break;
                                        case 9:
                                            color = 1;
                                            break;
                                        case 13:
                                            color = 5;
                                            break;
                                        case 11:
                                            color = 3;
                                            break;
                                        case 15:
                                            color = 7;
                                            break;
                                        default:
                                            break;
                                    }  
                                }                              
                                break;
                            case 30:
                                if (bold) {
                                    color = 8;
                                } else {
                                    color = 0;
                                }
                                break;
                            case 31:
                                if (bold) {
                                    color = 12;
                                } else {
                                    color = 4;
                                }
                                break;
                            case 32:
                                if (bold) {
                                    color = 10;
                                } else {
                                    color = 2;
                                }
                                break;
                            case 33:
                                if (bold) {
                                    color = 14;
                                } else {
                                    color = 6;
                                }
                                break;
                            case 34:
                                if (bold) {
                                    color = 9;
                                } else {
                                    color = 1;
                                }
                                break;
                            case 35:
                                if (bold) {
                                    color = 13;
                                } else {
                                    color = 5;
                                }
                                break;
                            case 36:
                                if (bold) {
                                    color = 11;
                                } else {
                                    color = 3;
                                }
                                break;
                            case 37:
                                if (bold) {
                                    color = 15;
                                } else {
                                    color = 7;
                                }
                                break;
                            case 40:
                                if (high_intensity_bg_enabled) {
                                    bgcolor = 8;
                                } else {
                                    bgcolor = 0;
                                }
                                break;
                            case 41:
                                if (high_intensity_bg_enabled) {
                                    bgcolor = 12;
                                } else {
                                    bgcolor = 4;
                                }
                                break;
                            case 42:
                                if (high_intensity_bg_enabled) {
                                    bgcolor = 10;
                                } else {
                                    bgcolor = 2;
                                }
                                break;
                            case 43:
                                if (high_intensity_bg_enabled) {
                                    bgcolor = 14;
                                } else {
                                    bgcolor = 6;
                                }
                                break;
                            case 44:
                                if (high_intensity_bg_enabled) {
                                    bgcolor = 9;
                                } else {
                                    bgcolor = 1;
                                }
                                break;
                            case 45:
                                if (high_intensity_bg_enabled) {
                                    bgcolor = 13;
                                } else {
                                    bgcolor = 5;
                                }
                                break;
                            case 46:
                                if (high_intensity_bg_enabled) {
                                    bgcolor = 11;
                                } else {
                                    bgcolor = 3;
                                }
                                break;
                            case 47:
                                if (high_intensity_bg_enabled) {
                                    bgcolor = 15;
                                } else {
                                    bgcolor = 7;
                                }
                                break;                                
                        }
                    }
                    state = 0;
                    break;
                case 'J':
					if (params[0] == 0) {
                        dest.x = 0;
                        dest.y = 16 * cur_row;
                        dest.w = 80 * 16;
                        dest.h = (25 - cur_row) * 16;
                        SDL_FillRect(screenbacker, &dest, bgcolor);
                        SDL_FillRect(blink_screen, &dest, bgcolor);
                        for (j=cur_row;j<25;j++) {
                            for (k=0;k<80;k++) {
                                screen_buffer[k][j] = ' ';
                            }
                        }
                        
					} else if (params[0] == 1) {
                        dest.x = 0;
                        dest.y = 0;
                        dest.w = 80 * 16;
                        dest.h = cur_row * 16;
                        SDL_FillRect(screenbacker, &dest, bgcolor);
                        SDL_FillRect(blink_screen, &dest, bgcolor);
                        for (j=0;j<cur_row;j++) {
                            for (k=0;k<80;k++) {
                                screen_buffer[k][j] = ' ';
                            }
                        }
					} else if (params[0] == 2) {
						SDL_FillRect(screenbacker, 0, bgcolor);
                        SDL_FillRect(blink_screen, 0, bgcolor);
                        for (j=0;j<75;j++) {
                            for (k=0;k<80;k++) {
                                scrollback_buffer[k][j] = scrollback_buffer[k][j+25];
                            }
                        }
                        for (j=0;j<25;j++) {
                            for (k=0;k<80;k++) {
                                scrollback_buffer[k][j+75] = screen_buffer[k][j];
                                screen_buffer[k][j] = ' ';
                                
                            }
                        }
						cur_col = 0;
						cur_row = 0;
					}
					state = 0;                
                    break;
                case 'K':
					if (params[0] == 0) {
						j = cur_col;
						for (k=j;k<80;k++) {
							drawChar(' ');
						}
						cur_col = j;
					} else if (params[0] == 1) {
						j = cur_col;
						cur_col = 0;
						for (k=0;k<j;k++) {
							drawChar(' ');
						}
						cur_col = j;
					} else if (params[0] == 2) {
						j = cur_col;
						for (k=0;k<80;k++) {
							drawChar(' ');
						}
						cur_col = j;
					}
					state = 0;
                    break;
                case 'n':
                    // report cursor position

					if (params[0] == 6) {
						if ((type == 0 && chan != NULL) || (type == 1 && tsock != 0)) {
							snprintf(buf, 10, "\x1b[%d;%dR", cur_row + 1, cur_col + 1);
							if (type == 0) {
								ssh_channel_write(chan, buf, strlen(buf));
							}
							else if (type == 1) {
								send(tsock, buf, strlen(buf), 0);
							}
						}
					}

					state = 0;
                    break;
                case '?':
                    vt320_count = 0;
                    for (j=0;j<16;j++) {
                        vt320[j] = 0;
                    }
                    state = 5;
                    break;
                case 27:
                    state = 1;
                    break;
                case ' ':
                    state = 7;
                    break;
                case '*':
                    state = 8;
                    break;
                default:
                   // printf ("Unknown ansi code %d\n", c[i]);
                    state = 0;
                    break;
            }
        } else if (state == 5) {
            if (c[i] == ';') {
				if (vt320_count < 15) {
					vt320_count++;
				}
				continue;
			} else if (c[i] >= '0' && c[i] <= '9') {
				if (!vt320_count) vt320_count = 1;
				vt320[vt320_count-1] = vt320[vt320_count-1] * 10 + (c[i] - '0');
				continue;
			} else {
				state = 6;
			}            
        } else if (state == 7) {
            if (c[i] == 'D') {
                switch(params[1]) {
                    case 37:
                        curfont = 1;
                        break;
                    case 40:
                        curfont = 2;
                        break;
                    case 39:
                        curfont = 3;
                        break;
                    case 41:
                        curfont = 4;
                        break;
                    case 42:
                        curfont = 5;
                        break;
                    case 38:
                        curfont = 6;
                        break;
                    default:
                        curfont = 0;
                        break;
                }
                //printf("Got font change request %d\n", params[1]);
            }
            state = 0;
        } else if (state == 8) {
            if (c[i] == 'r') {
                printf("Got Speed Emulation Change Request\n");
            }
            state = 0;
        }

        if (state == 6) {

            if (c[i] == 'h') {
                for (j=0;j<vt320_count;j++) {
                    switch (vt320[j]) {
                        case 25:
                            cursor = 1;
                            break;
                        case 33:
                            high_intensity_bg = 1;
                            break;
                        case 35:
                            blink_disabled = 1;
                            break;
                        default:
                            printf("Got unknown v320 code %d\n", vt320[j]);
                            break;
                    }
                }
            } else if (c[i] == 'l') {
                for (j=0;j<vt320_count;j++) {
                    switch (vt320[j]) {
                        case 25:
                            cursor = 0;
                            break;
                        case 33:
                            high_intensity_bg = 0;
                            break;
                        case 35:
                            blink_disabled = 0;
                            break;
                        default:
                            printf("Got unknown v320 code %d\n", vt320[j]);
                            break;
                    }
                }

            }
            state = 0;
        }
	}
    return;
}

#ifdef _MSC_VER
std::string utf16ToUTF8(const std::wstring &s)
{
	const int size = ::WideCharToMultiByte(CP_UTF8, 0, s.c_str(), -1, NULL, 0, 0, NULL);

	std::vector<char> buf(size);
	::WideCharToMultiByte(CP_UTF8, 0, s.c_str(), -1, &buf[0], size, 0, NULL);

	return std::string(&buf[0]);
}
#endif

void Terminal::setZmodem(int *z) {
    zmodem = z;
}


void Terminal::screenshot() {
    char sspath[PATH_MAX];
    char ssfile[PATH_MAX];
    struct passwd *pwd;
    struct tm *time_now;   
    time_t timen; 
    struct stat st;
    int i = 1;
   // const Uint32 format = SDL_PIXELFORMAT_ARGB8888;
    const int width = 640;
    const int height = 400;
    Uint32 rmask, gmask, bmask, amask;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    rmask = 0xff000000;
    gmask = 0x00ff0000;
    bmask = 0x0000ff00;
    amask = 0x000000ff;
#else
    rmask = 0x000000ff;
    gmask = 0x0000ff00;
    bmask = 0x00ff0000;
    amask = 0xff000000;
#endif    
    SDL_Surface *sshot = SDL_CreateRGBSurface(0, width, height, 32, rmask, gmask, bmask, amask);
    //SDL_Surface *sshot = SDL_CreateRGBSurfaceWithFormat(0, width, height, 32, format);

#ifdef _MSC_VER
	WCHAR *homedir;
	SHGetKnownFolderPath(FOLDERID_Pictures, 0, NULL, &homedir);
    snprintf(sspath, PATH_MAX, "%s", utf16ToUTF8(homedir).c_str());
#else
    pwd = getpwuid(getuid());
    snprintf(sspath, PATH_MAX, "%s", pwd->pw_dir);
#endif
    timen = time(NULL);

    if (stat(sspath, &st) != 0) {
        return;
    }

    time_now = localtime(&timen);
    snprintf(ssfile, PATH_MAX, "%s%cMagiTerm SS - %04d%02d%02d%02d%02d%02d.bmp", sspath, PATH_SEP, time_now->tm_year + 1900, time_now->tm_mon + 1, time_now->tm_mday, time_now->tm_hour, time_now->tm_min, time_now->tm_sec);
    while (stat(ssfile, &st) == 0) {
        snprintf(ssfile, PATH_MAX, "%s%cMagiTerm SS - %04d%02d%02d%02d%02d%02d-%d.bmp", sspath, PATH_SEP, time_now->tm_year + 1900, time_now->tm_mon + 1, time_now->tm_mday, time_now->tm_hour, time_now->tm_min, time_now->tm_sec, i);
        i++;
    }
    SDL_FillRect(sshot, 0, 0xFF000000);
    SDL_BlitSurface(screenbacker, 0, sshot, 0);
    SDL_SaveBMP(sshot, ssfile);
}

void Terminal::toggleIceColours() {
    icecolours = !icecolours;

    if (icecolours) {
        high_intensity_bg = 1;
        blink_disabled = 1;
    } else {
        high_intensity_bg = 0;
        blink_disabled = 0;
    }
}

int Terminal::isIceEnabled() {
    if (high_intensity_bg && blink_disabled) {
        return 1;
    } else {
        return 0;
    }
}

SDL_Surface *Terminal::getSurface() {
    SDL_Rect src, dest;
    src.x = 0;
    src.y = 0;
    src.w = 640;
    src.h = 400;

    dest.x = 0;
    dest.y = 0;
    dest.w = 640;
    dest.h = 400;
    SDL_FillRect(screen, &src, 0xff000000);
    if (last_show <= SDL_GetTicks()) {
        blink_toggle = !blink_toggle;
        last_show = SDL_GetTicks() + 500;
    }
    if (scrollingBack) {
        SDL_BlitSurface(scrollback_screen, &src, screen, &dest);
    } else {
        if (!blink_toggle) {
            SDL_BlitSurface(screenbacker, &src, screen, &dest);
        } else {
            SDL_BlitSurface(blink_screen, &src, screen, &dest);
        }
        if (cursor) {
            src.x = cur_col * 8;
            src.y = cur_row * 16 + 14;
            src.w = 8;
            src.h = 2;
            
            SDL_FillRect(screen, &src, 0xffffffff);
        }
    }
    return screen;
}

Terminal::~Terminal()
{
    //dtor
    SDL_FreeSurface(screen);
    SDL_FreeSurface(screenbacker);
    SDL_FreeSurface(screencopy);
    
    SDL_FreeSurface(font_bmp);
    SDL_FreeSurface(pot_noodle_bmp);
    SDL_FreeSurface(topaz_plus_bmp);
    SDL_FreeSurface(topaz_bmp);
    SDL_FreeSurface(mk_bmp);
    SDL_FreeSurface(mk_plus_bmp);
    SDL_FreeSurface(mosoul_bmp);
}

extern "C" int zm_update;
extern "C" int zm_type;
extern "C" int zm_value;
extern "C" char *zm_status;

extern "C" int ZXmitStr(u_char* str, int len, ZModem* info) {
	int i;
	int count;
	sz = 1;
	if (type == 0) {
		count = ssh_channel_write(info->chan, str, len);
		if (count < 0) {
			return ZmErrSys;
		}
	}
	else if (type == 1) {
		count = send_telnet(info->tsock, (char *)str, len, 0);
		if (count == 0) {
			return ZmErrSys;
		}
	}

	return 0;
}

extern "C" void ZIFlush(ZModem* info) {
}

extern "C" void ZOFlush(ZModem* info) {
}

extern "C" int ZAttn(ZModem* info) {
	char* ptr;

	if (info->attn == NULL)
		return 0;

	for (ptr = info->attn; *ptr != '\0'; ++ptr) {
		if (*ptr == ATTNBRK) {

		}
		else if (*ptr == ATTNPSE) {
#ifdef _MSC_VER
			Sleep(1000);
#else
			sleep(1);
#endif
		}
		else {
			sz = 1;
			if (type == 0) {
				ssh_channel_write(info->chan, ptr, 1);
			}
			else if (type == 1) {
				send_telnet(info->tsock, ptr, 1, 0);
			}
		}
	}
	return 0;
}

extern "C" void ZFlowControl(int onoff, ZModem* info) {
}

extern "C" void ZStatus(int type, int value, char* status) {
    if (zm_update == 0) {
        zm_type = type;
        zm_value = value;
        if (zm_status != NULL) {
            free(zm_status);
            zm_status = NULL;
        }
        if (status != NULL) {
            zm_status = strdup(status);
        }
        zm_update = 1;
    }    
}

extern "C" FILE* ZOpenFile(char* name, uint32_t crc, ZModem* info) {
	FILE* fptr;
	char fname[256];
	char ext[256];
	char upload_filename[4096];
#ifdef _MSC_VER
	WCHAR* homedir;

	_splitpath(name, NULL, NULL, fname, ext);
	SHGetKnownFolderPath(FOLDERID_Downloads, 0, NULL, &homedir);
	snprintf(upload_filename, sizeof upload_filename, "%s\\%s%s", utf16ToUTF8(homedir).c_str(), fname, ext);
#else 
	struct passwd* pw = getpwuid(getuid());

	const char* homedir = pw->pw_dir;

	snprintf(upload_filename, sizeof upload_filename, "%s/%s", homedir, basename(name));
	if (access(upload_filename, F_OK) == 0) {
		return NULL;
	}

#endif

	fptr = fopen(upload_filename, "wb");

	return fptr;
}

extern "C" int ZWriteFile(u_char* buffer, int len, FILE* file, ZModem* info) {
	return fwrite(buffer, 1, len, file) == len ? 0 : ZmErrSys;
}

extern "C" int ZCloseFile(ZModem* info) {
	fclose(info->file);
	return 0;
}

extern "C" void ZIdleStr(u_char* buffer, int len, ZModem* info) {
}

int doIO(ZModem* zm, int *ptr) {
	u_char buffer[8193 + 21];
	int len;
	int done = 0;
	time_t then, now;
	len = -1;

	while (!done) {
		
		then = time(NULL);
		do {
            if (*ptr == 0) {
                return 1;
            }
			if (type == 0) {
				len = ssh_channel_read_nonblocking(zm->chan, buffer, 8193, 0);
			}
			else if (type == 1) {
				len = recv_telnet(zm->tsock, (char*)buffer, 8193, 0);
			}
			if (len > 0 || zm->timeout == 0) {
				break;
			}
			else if ((type == 0 && len < 0) || (type == 1 && len == 0)) {
				return ZmErrCancel;
			}
			if ((type == 0 && len == 0) || (type == 1 && len < 0)) {
#ifdef _MSC_VER
				Sleep(100);
#else
				usleep(100000);
#endif
			}
			now = time(NULL);
		} while ((now - then) < zm->timeout);
		if ((type == 0 && len == 0) || (type == 1 && len < 0)) {
			done = ZmodemTimeout(zm);
		}
		else if (len > 0) {
			rz = 1;
			done = ZmodemRcv(buffer, len, zm);
		}
	}
	return done;
}

int do_zmodem_download(void* ptr) {
	struct zmodem_info* zi = (struct zmodem_info*)ptr;
	int* zmodem = zi->ptr;
	ZModem zm;
	ft = 1;
	zm.attn = NULL;
	zm.windowsize = 0;
	zm.bufsize = 0;

	if (type == 0) {
		zm.chan = chan;
	}
	else if (type == 1) {
		zm.tsock = tsock;
	}

	zm.zrinitflags = 0;
	zm.zsinitflags = 0;

	zm.packetsize = 1024;

	ZmodemRInit(&zm);

	doIO(&zm, zmodem);

	free(zm.buffer);
	*zmodem = 0;
	ft = 0;
	return 0;
}

int do_zmodem_upload(void* ptr) {
	struct zmodem_info* zi = (struct zmodem_info*)ptr;
	int* zmodem = zi->ptr;
	ZModem zm;
	
	ft = 1;
	int done;
	char fname[256];
	char ext[256];
	char fnamebuf[513];
	zm.attn = NULL;
	zm.windowsize = 2048;
	zm.packetsize = 1024;

	if (type == 1) {
		zm.tsock = tsock;
	}
	else if (type == 0) {
		zm.chan = chan;
	}
	zm.zrinitflags = 0;
	zm.zsinitflags = 0;

	zm.packetsize = 1024;

	ZmodemTInit(&zm);
	//done = doIO(&zm);
	//if (done != ZmDone) {
	//	ft = 0;
	//	*zmodem = 0;
    //	return 0;
	//}

#ifdef _MSC_VER
	_splitpath(zi->uploadfile, NULL, NULL, fname, ext);
	snprintf(fnamebuf, sizeof fnamebuf, "%s%s", fname, ext);
	done = ZmodemTFile(zi->uploadfile, fnamebuf, ZCBIN, 0, 0, 0, 1, 0, &zm);
#else 
	done = ZmodemTFile(zi->uploadfile, basename(zi->uploadfile), ZCBIN, 0, 0, 0, 1, 0, &zm);
#endif
	switch (done) {
	case 0:
		break;

	case ZmErrCantOpen:
		ft = 0;
		*zmodem = 0;
		return 0;

	case ZmFileTooLong:
		ft = 0;
		*zmodem = 0;
		return 0;

	case ZmDone:
		ft = 0;
		*zmodem = 0;
		return 0;

	default:
		ft = 0;
		*zmodem = 0;
		return 0;
	}

	done = doIO(&zm, zmodem);

	if (done != ZmDone) {
		ft = 0;
		*zmodem = 0;
		return 0;
	}

	done = ZmodemTFinish(&zm);

	if (!done) {
		done = doIO(&zm, zmodem);
	}
	*zmodem = 0;
	ft = 0;
	return 0;
}
